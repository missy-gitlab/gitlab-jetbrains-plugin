package com.gitlab.plugin.codesuggestions.rules

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.codeInsight.inline.completion.session.InlineCompletionContext
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.editor.Editor
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.mockk.*

class CurrentlyDisplayingCompletionTest : DescribeSpec({
  mockkStatic(InlineCompletionContext::getOrNull)

  val logger: Logger = mockk()
  val request: InlineCompletionRequest = mockk()
  val editor: Editor = mockk()
  val currentlyDisplayingCompletion = CurrentlyDisplayingCompletion(logger)

  beforeEach {
    every { logger.info(any<String>()) } just runs
    every { request.editor } returns editor
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  context("when completion is currently displaying") {
    beforeEach {
      every { InlineCompletionContext.getOrNull(editor)?.isCurrentlyDisplaying() } returns true
    }

    it("returns true") {
      currentlyDisplayingCompletion.shouldSkipSuggestion(request) shouldBe true
    }

    it("logs a message") {
      val logMessageSlot = slot<String>()
      every { logger.info(capture(logMessageSlot)) } just runs

      currentlyDisplayingCompletion.shouldSkipSuggestion(request)

      verify(exactly = 1) { logger.info(any<String>()) }
      logMessageSlot.captured shouldContain "completion is currently displaying"
    }
  }

  context("when completion is not currently displaying") {
    beforeEach {
      every { InlineCompletionContext.getOrNull(editor)?.isCurrentlyDisplaying() } returns false
    }

    it("returns false") {
      currentlyDisplayingCompletion.shouldSkipSuggestion(request) shouldBe false
    }

    it("does not log a message") {
      currentlyDisplayingCompletion.shouldSkipSuggestion(request)

      verify(exactly = 0) { logger.info(any<String>()) }
    }
  }

  context("when InlineCompletionContext is null") {
    beforeEach {
      every { InlineCompletionContext.getOrNull(editor) } returns null
    }

    it("returns false") {
      currentlyDisplayingCompletion.shouldSkipSuggestion(request) shouldBe false
    }

    it("does not log a message") {
      currentlyDisplayingCompletion.shouldSkipSuggestion(request)

      verify(exactly = 0) { logger.info(any<String>()) }
    }
  }
})
