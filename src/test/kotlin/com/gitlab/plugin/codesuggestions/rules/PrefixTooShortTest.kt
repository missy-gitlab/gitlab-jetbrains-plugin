package com.gitlab.plugin.codesuggestions.rules

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.diagnostic.Logger
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.mockk.*

class PrefixTooShortTest : DescribeSpec({
  val request: InlineCompletionRequest = mockk()
  val logger: Logger = mockk()
  val prefixTooShort = PrefixTooShort(logger)

  beforeEach {
    every { request.document.charsSequence } returns "01234567890123"
    every { logger.info(any<String>()) } just runs
  }

  afterEach {
    clearMocks(request, logger)
  }

  context("when there are not enough characters before the cursor") {
    beforeEach {
      every { request.endOffset } returns PrefixTooShort.MIN_CHARS
    }

    it("returns true") {
      prefixTooShort.shouldSkipSuggestion(request) shouldBe true
    }

    it("logs a message") {
      val logMessageSlot = slot<String>()
      every { logger.info(capture(logMessageSlot)) } just runs

      prefixTooShort.shouldSkipSuggestion(request)

      verify(exactly = 1) { logger.info(any<String>()) }
      logMessageSlot.captured shouldContain "less than ${PrefixTooShort.MIN_CHARS} characters before the cursor"
    }
  }

  context("when there are enough characters before the cursor") {
    beforeEach {
      every { request.endOffset } returns PrefixTooShort.MIN_CHARS + 1
    }

    it("returns false") {
      prefixTooShort.shouldSkipSuggestion(request) shouldBe false
    }

    it("does not log a message") {
      prefixTooShort.shouldSkipSuggestion(request)

      verify(exactly = 0) { logger.info(any<String>()) }
    }
  }
})
