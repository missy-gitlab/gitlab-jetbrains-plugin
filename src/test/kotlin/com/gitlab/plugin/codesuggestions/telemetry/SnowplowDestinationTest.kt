package com.gitlab.plugin.codesuggestions.telemetry

import com.snowplowanalytics.snowplow.tracker.Tracker
import com.snowplowanalytics.snowplow.tracker.events.Structured
import com.snowplowanalytics.snowplow.tracker.payload.SelfDescribingJson
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.maps.shouldContainAll
import io.kotest.matchers.shouldBe
import io.mockk.*

class SnowplowDestinationTest : DescribeSpec({
  mockkObject(IdeExtensionVersionContext, CodeSuggestionsContext)

  val tracker: Tracker = mockk()
  val snowplowDestination = SnowplowDestination(tracker)

  beforeEach {
    every { IdeExtensionVersionContext.build() } returns SelfDescribingJson(
      "ide_extension_version",
      mapOf("test-data" to "ide")
    )
    every { CodeSuggestionsContext.build(any()) } returns SelfDescribingJson(
      "code_suggestions_context",
      mapOf("test-data" to "code")
    )
    every { tracker.track(any()) } returns emptyList()
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("build") {
    it("tracks an event") {
      snowplowDestination.event(Event(Event.Type.ERROR, Event.Context(uuid = "test-uuid")))

      verify(exactly = 1) { tracker.track(any()) }
    }

    it("creates an event with the correct payload") {
      val structuredEventSlot = slot<Structured>()
      every { tracker.track(capture(structuredEventSlot)) } returns emptyList()

      snowplowDestination.event(Event(Event.Type.ERROR, Event.Context(uuid = "test-uuid")))

      structuredEventSlot.captured.payload.map shouldContainAll mapOf(
        "se_ca" to SnowplowDestination.EVENT_CATEGORY,
        "se_ac" to Event.Type.ERROR.action,
        "se_la" to "test-uuid",
      )
    }

    it("creates an event with the correct contexts") {
      val structuredEventSlot = slot<Structured>()
      every { tracker.track(capture(structuredEventSlot)) } returns emptyList()

      snowplowDestination.event(Event(Event.Type.ERROR, Event.Context(uuid = "test-uuid")))

      structuredEventSlot.captured.context.size shouldBe 2
      structuredEventSlot.captured.context.first().map shouldContainAll mapOf(
        "schema" to "ide_extension_version",
        "data" to mapOf(
          "test-data" to "ide"
        )
      )
      structuredEventSlot.captured.context.last().map shouldContainAll mapOf(
        "schema" to "code_suggestions_context",
        "data" to mapOf(
          "test-data" to "code"
        )
      )
    }
  }
})
