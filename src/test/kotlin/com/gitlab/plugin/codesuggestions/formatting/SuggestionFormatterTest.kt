package com.gitlab.plugin.codesuggestions.formatting

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.codesuggestions.SuggestionContext
import com.intellij.lang.Language
import com.intellij.testFramework.LightPlatformTestCase
import com.intellij.testFramework.runInEdtAndWait
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

/*
 * HTML is used for the tests as it is a language supported by
 * GitLab code suggestions and installed by default in the platform.
 */
class SuggestionFormatterTest : LightPlatformTestCase() {
  private val suggestionFormatter = SuggestionFormatter()
  private val settings = mockk<DuoPersistentSettings>()

  @BeforeEach
  override fun setUp() {
    super.setUp()

    every { settings.suggestionFormattingEnabled } returns true
  }

  @AfterEach
  override fun tearDown() {
    runInEdtAndWait { super.tearDown() }

    clearAllMocks()
  }

  @Test
  fun `it should not format suggestion if it is inline`() {
    val prefix = "<html>\n"
    val suggestion = "<h1>Hello</h1>"
    val suffix = "\n</html>"

    val result = runBlocking {
      suggestionFormatter.format(
        suggestion = suggestion,
        context = suggestionContext(prefix, suffix, isCurrentLineEmpty = true),
        settings = settings
      )
    }

    result shouldBe suggestion
  }

  @Test
  fun `it should throw when on empty suggestion`() {
    val prefix = "<html>\n"
    val suggestion = ""
    val suffix = "\n</html>"

    val result = shouldNotThrowAny {
      runBlocking {
        suggestionFormatter.format(
          suggestion = suggestion,
          context = suggestionContext(prefix, suffix, isCurrentLineEmpty = true),
          settings = settings
        )
      }
    }

    result shouldBe suggestion
  }

  @Test
  fun `given project is null, it should only trim suggestion`() {
    val prefix = ""
    val suggestion = "\n\t\n <div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>\n\t"
    val suffix = ""
    val context = suggestionContext(prefix, suffix, isCurrentLineEmpty = true).apply {
      every { project } returns null
    }

    val result = runBlocking {
      suggestionFormatter.format(
        suggestion = suggestion,
        context = context,
        settings = settings
      )
    }

    result shouldBe "<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>"
  }

  @Test
  fun `it should not format suggestion if the suggestion formatting setting is disabled`() {
    every { settings.suggestionFormattingEnabled } returns false
    val prefix = ""
    val suggestion = "<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>"
    val suffix = ""

    val result = runBlocking {
      suggestionFormatter.format(
        suggestion = suggestion,
        context = suggestionContext(prefix, suffix, isCurrentLineEmpty = true),
        settings = settings
      )
    }

    result shouldBe suggestion
  }

  @Test
  fun `it should still attempt to format incomplete code`() {
    val prefix = "\n"
    val suggestion = "<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>"
    val suffix = ""

    val result = runBlocking {
      suggestionFormatter.format(
        suggestion = suggestion,
        context = suggestionContext(prefix, suffix, isCurrentLineEmpty = true),
        settings = settings
      )
    }

    result shouldBe "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>"
  }

  /*
   * The CodeStyleManager will do it's best to indent the code using the AST even if the code is invalid.
   * The quality of the result will depend on how broken the code is.
   */
  @Test
  fun `it should still attempt to format invalid code`() {
    val prefix = "\n"
    // Invalid h1 closing tag and no p closing tag.
    val suggestion = "<div>\n<h1>Hello</h2>\n<p>Tests are running...\n</div>"
    val suffix = ""

    val result = runBlocking {
      suggestionFormatter.format(
        suggestion = suggestion,
        context = suggestionContext(prefix, suffix, isCurrentLineEmpty = true),
        settings = settings
      )
    }

    result shouldBe "<div>\n" +
      "    <h1>Hello</h2>\n" +
      "        <p>Tests are running...\n" +
      "</div>"
  }

  @Test
  fun `it should trim start and end of suggestion if it starts on empty line`() {
    val prefix = ""
    val suggestion = "\n\t<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>\n\t    "
    val suffix = ""

    val result = runBlocking {
      suggestionFormatter.format(
        suggestion = suggestion,
        context = suggestionContext(prefix, suffix, isCurrentLineEmpty = true),
        settings = settings
      )
    }

    result shouldBe "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>"
  }

  @Test
  fun `it should only trim the end of suggestion if it starts on a non-empty line`() {
    val prefix = "<html>\n<bo"
    val suggestion = "dy>\n<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>\n</body>\n\n\n\n\t    "
    val suffix = "\n</html>"

    val result = runBlocking {
      suggestionFormatter.format(
        suggestion = suggestion,
        context = suggestionContext(prefix, suffix, isCurrentLineEmpty = false),
        settings = settings
      )
    }

    result shouldBe "dy>\n" +
      "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>\n" +
      "</body>"
  }

  @Test
  fun `given prefix and suffix, it should indent all suggestion lines except the current one`() {
    val prefix = "<html>\n<bo"
    val suggestion = "dy>\n<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>\n</body>"
    val suffix = "\n</html>"

    val result = runBlocking {
      suggestionFormatter.format(
        suggestion = suggestion,
        context = suggestionContext(prefix, suffix, isCurrentLineEmpty = false),
        settings = settings
      )
    }

    result shouldBe "dy>\n" +
      "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>\n" +
      "</body>"
  }

  @Test
  fun `given no prefix and no suffix, it should indent all suggestion lines except the current one`() {
    val prefix = ""
    val suggestion = "<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>"
    val suffix = ""

    val result = runBlocking {
      suggestionFormatter.format(
        suggestion = suggestion,
        context = suggestionContext(prefix, suffix, isCurrentLineEmpty = true),
        settings = settings
      )
    }

    result shouldBe "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>"
  }

  @Test
  fun `given prefix and no suffix, it should indent all suggestion lines except the current one`() {
    val prefix = "<div>\n<h1>"
    val suggestion = "Hello</h1>\n<p>Tests are running...</p>\n</div>"
    val suffix = ""

    val result = runBlocking {
      suggestionFormatter.format(
        suggestion = suggestion,
        context = suggestionContext(prefix, suffix, isCurrentLineEmpty = false),
        settings = settings
      )
    }

    result shouldBe "Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>"
  }

  @Test
  fun `given no prefix and suffix, it should indent all suggestion lines except the current one`() {
    val prefix = ""
    val suggestion = "<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>"
    val suffix = "\n<!-- trailing comment -->"

    val result = runBlocking {
      suggestionFormatter.format(
        suggestion = suggestion,
        context = suggestionContext(prefix, suffix, isCurrentLineEmpty = true),
        settings = settings
      )
    }

    result shouldBe "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>"
  }

  private fun suggestionContext(prefix: String, suffix: String, isCurrentLineEmpty: Boolean) = mockk<SuggestionContext> {
    every { project } returns getProject()
    every { this@mockk.prefix } returns prefix
    every { this@mockk.suffix } returns suffix
    every { this@mockk.isCurrentLineEmpty } returns isCurrentLineEmpty
    every { extension } returns "html"
    every { language } returns Language.findLanguageByID("HTML")!!
  }

  override fun getName(): String {
    return javaClass.simpleName
  }
}
