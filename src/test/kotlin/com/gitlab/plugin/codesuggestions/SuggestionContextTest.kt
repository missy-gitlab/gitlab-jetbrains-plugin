package com.gitlab.plugin.codesuggestions

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk

@Suppress("UnstableApiUsage")
class SuggestionContextTest : DescribeSpec({
  it("should extract prefix and suffix from document") {
    val request = mockk<InlineCompletionRequest> {
      every { document } returns mockk {
        every { charsSequence } returns "prefix__suffix"
      }
      every { endOffset } returns 7
    }

    val context = SuggestionContext(request)

    context.prefix shouldBe "prefix_"
    context.suffix shouldBe "_suffix"
  }

  it("should have current has empty if it is empty") {
    val request = mockk<InlineCompletionRequest>(relaxed = true) {
      every { document.text } returns "package main\n         "
      every { document.getLineNumber(23) } returns 1
      every { endOffset } returns 23
    }

    val context = SuggestionContext(request)

    context.isCurrentLineEmpty shouldBe true
  }

  it("should not have current has empty if it is not empty") {
    val request = mockk<InlineCompletionRequest>(relaxed = true) {
      every { document.text } returns "package main\nfunc main"
      every { document.getLineNumber(23) } returns 1
      every { endOffset } returns 23
    }

    val context = SuggestionContext(request)

    context.isCurrentLineEmpty shouldBe false
  }

  it("should create request payload based on request information") {
    val request = mockk<InlineCompletionRequest>(relaxed = true) {
      every { document.charsSequence } returns "prefix__suffix"
      every { file.virtualFile.path } returns "src/main.kt"
      every { editor.project?.basePath } returns "src/"
      every { endOffset } returns 7
    }

    val payload = SuggestionContext(request).toRequestPayload()

    payload.currentFile.fileName shouldBe "main.kt"
    payload.currentFile.contentAboveCursor shouldBe "prefix_"
    payload.currentFile.contentBelowCursor shouldBe "_suffix"
    payload.telemetry.isEmpty() shouldBe true
  }
})
