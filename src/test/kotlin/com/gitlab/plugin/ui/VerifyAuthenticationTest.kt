package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.buildMockEngine
import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.api.pat.PatInfo
import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.services.PatStatusService
import com.gitlab.plugin.services.pat.PatStatus
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.openapi.application.Application
import com.intellij.openapi.application.ApplicationManager
import com.intellij.ui.components.BrowserLink
import com.intellij.ui.dsl.builder.Cell
import com.intellij.util.net.HttpConfigurable
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.ktor.client.engine.*
import io.ktor.client.engine.mock.*
import io.ktor.http.*
import io.mockk.*
import java.nio.channels.UnresolvedAddressException
import javax.swing.JLabel

class VerifyAuthenticationTest : DescribeSpec({
  val host = "https://gitlab.com/"
  val token = "token"
  val label = JLabel()
  val errorReportingInstructions: Cell<BrowserLink> = mockk(relaxed = true)

  val validPat = PatInfo(
    id = 1,
    name = "name",
    revoked = false,
    createdAt = "2022-01-01T00:00:00Z",
    expiresAt = "2022-01-01T00:00:00Z",
    scopes = listOf(GitLabUtil.API_PAT_SCOPE),
    userId = 123,
    lastUsedAt = "2022-01-01T00:00:00Z",
    active = true
  )

  val inactivePat = validPat.copy(active = false)

  val patStatusService = mockk<PatStatusService>()

  beforeTest {
    mockkStatic(HttpConfigurable::class)
    every { HttpConfigurable.getInstance() } returns mockk<HttpConfigurable>(relaxed = true)

    val application = mockk<Application> {
      every { getService(any<Class<*>>()) } answers { callOriginal() }
      every { getService(PatStatusService::class.java) } returns patStatusService
    }

    mockkStatic(ApplicationManager::class)
    every { ApplicationManager.getApplication() } returns application

    mockkObject(DuoContextService)
    mockkObject(GitLabBundle)

    every { DuoContextService.instance.duoHttpClient } returns DuoClient(
      host = host,
      tokenProvider = { token },
      exceptionHandler = { _ -> },
      userAgent = "gitlab-duo-plugin",
      shouldRetry = false,
      onStatusChanged = mockk(relaxed = true),
      httpClientEngine = MockEngine { throw UnresolvedAddressException() }
    )
  }

  beforeEach {
    label.text = ""
  }

  afterEach {
    clearAllMocks()
  }

  afterTest {
    unmockkAll()
  }

  describe("verifyServerConfiguration") {
    it("should display server cannot be reached message when verification server is offline") {
      MockEngine { throw UnresolvedAddressException() }.applyToHttpClient()

      verifyServerConfiguration(host, token, label, errorReportingInstructions)

      label.text shouldBe htmlErrorMessage("Server cannot be reached.")
    }

    it("should display pat is insufficient message when verification responds with forbidden") {
      buildMockEngine(HttpStatusCode.Forbidden, "Error").applyToHttpClient()

      verifyServerConfiguration(host, token, label, errorReportingInstructions)

      label.text shouldBe htmlErrorMessage("Personal Access Token is insufficient.")
    }

    it("should display duo unavailable message when verification responds with unauthorized") {
      buildMockEngine(HttpStatusCode.Unauthorized, "Error").applyToHttpClient()

      verifyServerConfiguration(host, token, label, errorReportingInstructions)

      label.text shouldBe htmlErrorMessage("Personal Access Token is invalid or Code Suggestions is not available on your instance.")
    }

    it("should display invalid proxy setup message when http client proxy is not setup correctly") {
      buildMockEngine(HttpStatusCode.PayloadTooLarge, "Error").applyToHttpClient()

      verifyServerConfiguration(host, token, label, errorReportingInstructions)

      label.text shouldBe htmlErrorMessage("Proxy is not setup correctly.")
    }

    it("should display not licensed message when gitlab.com responds with not found") {
      buildMockEngine(HttpStatusCode.NotFound, "Error").applyToHttpClient()
      every { GitLabBundle.message("code-suggestions.not-licensed") } returns "Not Licensed"

      verifyServerConfiguration(host, token, label, errorReportingInstructions)

      label.text shouldBe htmlErrorMessage("Not Licensed")
    }

    it("should display not licensed message when self-hosted instance responds with an error") {
      buildMockEngine(HttpStatusCode.NotFound, "Error").applyToHttpClient()

      verifyServerConfiguration("https://self-hosted.gitlab.com/", token, label, errorReportingInstructions)

      label.text shouldBe htmlErrorMessage(
        "An unexpected error occurred. See the error notification for more details, or click the link below to learn how to report this error."
      )
      verify { errorReportingInstructions.visible(true) }
    }

    it("should display pat status message if pat is invalid") {
      buildMockEngine(HttpStatusCode.OK, "{}").applyToHttpClient()
      coEvery { patStatusService.currentStatus(host, token) } returns PatStatus.Accepted(inactivePat)

      verifyServerConfiguration(host, token, label, errorReportingInstructions)

      label.text shouldBe "Personal Access Token is not active. Try again with an active token."
    }

    it("should display success message if pat is valid") {
      buildMockEngine(HttpStatusCode.OK, "{}").applyToHttpClient()
      coEvery { patStatusService.currentStatus(host, token) } returns PatStatus.Accepted(validPat)

      verifyServerConfiguration(host, token, label, errorReportingInstructions)

      label.text shouldBe "Success! The server host and Personal Access Token are correctly setup."
    }
  }
})

private fun htmlErrorMessage(message: String): String {
  return "<html><body style='width: 350px;'>$message</body></html>"
}

private fun HttpClientEngine.applyToHttpClient() {
  val httpClient = DuoContextService.instance.duoHttpClient.copy(httpClientEngine = this)

  every { DuoContextService.instance.duoHttpClient } returns httpClient
}
