package com.gitlab.plugin.ui

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.codesuggestions.license.CodeSuggestionsLicenseStatus
import com.gitlab.plugin.services.GitLabServerService
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.util.Status
import com.gitlab.plugin.util.TokenUtil
import com.intellij.notification.Notification
import com.intellij.notification.NotificationType
import com.intellij.notification.Notifications
import com.intellij.openapi.application.Application
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.wm.StatusBar
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import io.mockk.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.awt.event.MouseEvent
import java.util.concurrent.CompletableFuture

class GitLabStatusPanelTest : BasePlatformTestCase() {
  private companion object {
    val MOUSE_EVENT = mockk<MouseEvent>()
  }

  init {
    mockkObject(ProjectContextService)
  }

  private val statusBar = mockk<StatusBar>(relaxed = true)
  private val receivedNotifications = mutableListOf<Notification>()

  private lateinit var panel: GitLabStatusPanel

  @BeforeEach
  public override fun setUp() {
    super.setUp()

    val application = setupSingleThreadedApplication()
    application.messageBus.connect(testRootDisposable).subscribe(
      Notifications.TOPIC,
      object : Notifications {
        override fun notify(notification: Notification) {
          receivedNotifications.add(notification)
        }
      }
    )

    panel = GitLabStatusPanel(project)
    panel.install(statusBar)
  }

  @AfterEach
  fun teardown() {
    super.tearDown()

    clearAllMocks()
    unmockkAll()

    DuoPersistentSettings.getInstance().loadState(DuoPersistentSettings.State())
  }

  @Test
  fun `shows license missing notification when the user is not licensed`() {
    every { ProjectContextService.instance.codeSuggestionsLicense.isLicensed } returns false

    panel.getClickConsumer().consume(MOUSE_EVENT)

    val receivedNotification = receivedNotifications.last()
    val expectedNotification = CodeSuggestionsLicenseStatus.licenseMissingNotification()
    assertEquals(receivedNotification.title, expectedNotification.title)
    assertEquals(receivedNotification.content, expectedNotification.message)
    assertEquals(receivedNotification.type, NotificationType.INFORMATION)

    verify { statusBar.updateWidget(panel.ID()) }
  }

  @Test
  fun `shows license missing notification when the version is unsupported`() {
    every { ProjectContextService.instance.codeSuggestionsLicense.isLicensed } returns true
    every { ProjectContextService.instance.serverServiceWithoutCallbacks.get().isVersionUpgradeRequired } returns true

    panel.getClickConsumer().consume(MOUSE_EVENT)

    val receivedNotification = receivedNotifications.last()
    val expectedNotification = GitLabServerService.versionUnsupportedNotification()
    assertEquals(receivedNotification.title, expectedNotification.title)
    assertEquals(receivedNotification.content, expectedNotification.message)
    assertEquals(receivedNotification.type, NotificationType.INFORMATION)

    verify { statusBar.updateWidget(panel.ID()) }
  }

  @Test
  fun `disables Duo if Duo features are enabled when status is clicked`() {
    every { ProjectContextService.instance.codeSuggestionsLicense.isLicensed } returns true
    every { ProjectContextService.instance.serverServiceWithoutCallbacks.get().isVersionUpgradeRequired } returns false
    TokenUtil.setToken("token")

    panel.getClickConsumer().consume(MOUSE_EVENT)

    assertEquals(panel.status, Status.DISABLED)
    verify { statusBar.updateWidget(panel.ID()) }
  }

  @Test
  fun `enables Duo if Duo features are disabled when status is clicked`() {
    every { ProjectContextService.instance.codeSuggestionsLicense.isLicensed } returns true
    every { ProjectContextService.instance.serverServiceWithoutCallbacks.get().isVersionUpgradeRequired } returns false
    TokenUtil.setToken("token")
    DuoPersistentSettings.getInstance().toggleEnabled()

    panel.getClickConsumer().consume(MOUSE_EVENT)

    assertEquals(panel.status, Status.ENABLED)
    verify { statusBar.updateWidget(panel.ID()) }
  }

  @Test
  fun `shows error notification if Duo is not configured when status is clicked`() {
    every { ProjectContextService.instance.codeSuggestionsLicense.isLicensed } returns true
    every { ProjectContextService.instance.serverServiceWithoutCallbacks.get().isVersionUpgradeRequired } returns false
    TokenUtil.setToken("")

    panel.getClickConsumer().consume(MOUSE_EVENT)

    val receivedNotification = receivedNotifications.last()
    assertEquals(receivedNotification.title, "GitLab Duo")
    assertEquals(receivedNotification.content, "You need to configure your GitLab credentials first.")
    assertEquals(receivedNotification.type, NotificationType.INFORMATION)

    assertEquals(panel.status, Status.ERROR)
    verify { statusBar.updateWidget(panel.ID()) }
  }

  /*
    Some of the click code runs in an uncontrolled asynchronous thread pool owned by the application.
    Mocking the executeOnPooledThread and invokeLater methods enforces synchronous execution on the test thread.
   */
  private fun setupSingleThreadedApplication(): Application {
    val application = spyk(ApplicationManager.getApplication())

    mockkStatic(ApplicationManager::class)
    every { ApplicationManager.getApplication() } returns application

    every { application.executeOnPooledThread(any<Runnable>()) } answers {
      (invocation.args[0] as Runnable).run()

      CompletableFuture.completedFuture(null)
    }

    every { application.invokeLater(any<Runnable>()) } answers {
      (invocation.args[0] as Runnable).run()
    }

    return application
  }
}
