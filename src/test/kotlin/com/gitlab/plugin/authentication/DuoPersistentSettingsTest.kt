package com.gitlab.plugin.authentication

import io.kotest.core.spec.style.DescribeSpec
import io.kotest.datatest.withData
import io.kotest.matchers.shouldBe

@Suppress("HttpUrlsUsage")
class DuoPersistentSettingsTest : DescribeSpec({
  val settings = DuoPersistentSettings()

  describe("gitlabRealm") {
    context("returns the correct realm for SaaS URLs") {
      withData(listOf("https://gitlab.com", "http://gitlab.com")) {
        settings.url = it
        settings.gitlabRealm() shouldBe DuoPersistentSettings.GITLAB_REALM_SAAS
      }
    }

    context("returns the correct realm for self-managed URLs") {
      withData(
        listOf(
          "https://my-gitlab.com",
          "http://my-gitlab.com",
          "https://gitlab-for-me.com",
          "http://gitlab-for-me.com"
        )
      ) {
        settings.url = it
        settings.gitlabRealm() shouldBe DuoPersistentSettings.GITLAB_REALM_SELF_MANAGED
      }
    }
  }

  describe("suggestionFormattingEnabled") {
    beforeEach {
      settings.state.suggestionFormattingEnabled = DuoPersistentSettings.State().suggestionFormattingEnabled
    }

    it("defaults to true") {
      DuoPersistentSettings().suggestionFormattingEnabled shouldBe true
    }

    it("saves suggestionFormattingEnabled to state") {
      settings.suggestionFormattingEnabled = false

      settings.state.suggestionFormattingEnabled shouldBe false
    }
  }
})
