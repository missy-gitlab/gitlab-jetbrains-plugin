package com.gitlab.plugin.util

import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.workspace.DidChangeConfigurationParams
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class TokenUpdatedDidChangeConfigurationListenerTest : DescribeSpec({
  val userService = mockk<GitLabUserService>(relaxUnitFun = true)
  val listener = TokenUpdatedDidChangeConfigurationListener(userService)

  beforeEach {
    mockkObject(ProjectContextService)

    mockkStatic(::gitlabStatusRefresh)
    every { gitlabStatusRefresh() } answers {}

    mockkObject(TokenUtil)
    every { TokenUtil.setToken(any()) } returns Unit
  }

  afterEach {
    clearAllMocks()
    unmockkAll()
  }

  it("updates TokenUtil token when keychain token is configured") {
    val settings = WorkspaceSettingsBuilder { token("keychain_token") }

    listener.onConfigurationChange(DidChangeConfigurationParams(settings))

    verify { TokenUtil.setToken("keychain_token") }
  }

  it("updates gitlab status when project is initialized") {
    every { ProjectContextService.isInitialized() } returns true
    val settings = WorkspaceSettingsBuilder {}

    listener.onConfigurationChange(DidChangeConfigurationParams(settings))

    verify { gitlabStatusRefresh() }
  }

  it("doesn't update gitlab status when project is not initialized") {
    every { ProjectContextService.isInitialized() } returns false
    val settings = WorkspaceSettingsBuilder {}

    listener.onConfigurationChange(DidChangeConfigurationParams(settings))

    verify(exactly = 0) { gitlabStatusRefresh() }
  }
})
