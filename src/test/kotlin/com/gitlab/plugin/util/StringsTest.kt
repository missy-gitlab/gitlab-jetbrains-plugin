package com.gitlab.plugin.util

import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class StringsTest {
  @Nested
  inner class RemovePrefix {
    @Test
    fun `When string and prefix are given, returns string without prefix`() {
      val str = "/path/to/your/project/src/file.txt"
      val prefix = "/path/to/your/project"

      assertEquals("/src/file.txt", removePrefix(str, prefix))
    }

    @Test
    fun `When only string is given, returns entire string`() {
      val str = "/path/to/your/project/src/file.txt"
      val prefix: String? = null

      assertEquals(str, removePrefix(str, prefix))
    }

    @Test
    fun `When only prefix is given, returns null`() {
      val str: String? = null
      val prefix = "/path/to/your/project"

      assertEquals(null, removePrefix(str, prefix))
    }
  }
}
