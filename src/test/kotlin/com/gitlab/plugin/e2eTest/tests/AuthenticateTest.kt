package com.gitlab.plugin.e2eTest.tests

import com.automation.remarks.junit5.Video
import com.gitlab.plugin.e2eTest.pages.duoSettings
import com.gitlab.plugin.e2eTest.pages.idea
import com.gitlab.plugin.e2eTest.steps.ProjectSteps
import com.gitlab.plugin.e2eTest.utils.RemoteRobotExtension
import com.gitlab.plugin.e2eTest.utils.StepsLogger
import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.fixtures.JLabelFixture
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.waitForIgnoringError
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import java.time.Duration.ofMinutes

@Suppress("ClassName")
@ExtendWith(RemoteRobotExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuthenticateTest {
  val gitlabHost: String = System.getenv("TEST_GITLAB_HOST") ?: "https://staging.gitlab.com"

  init {
    StepsLogger.init()
  }

  @BeforeAll
  fun waitForIde(remoteRobot: RemoteRobot) {
    waitForIgnoringError(ofMinutes(3)) { remoteRobot.callJs("true") }
    ProjectSteps(remoteRobot).createNewProject()
    remoteRobot.idea { turnOffKeychainSettings() }
  }

  @AfterAll
  fun closeProject(remoteRobot: RemoteRobot) = ProjectSteps(remoteRobot).closeProject()

  @Nested
  inner class `with invalid token set` {
    @Test
    @Video
    fun `authentication error displayed`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        openDuoSettings()

        duoSettings { authenticate(gitlabHost, "glpat-FOO") }

        assert(
          jLabel(JLabelFixture.byContainsText("Personal Access Token is invalid")).isVisible()
        )

        button("Cancel").click()
      }
    }
  }

  @Nested
  inner class `with valid token set` {

    @Test
    @Video
    fun `GitLab Duo is enabled`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      val token: String? = System.getenv("TEST_ACCESS_TOKEN")

      check(!token.isNullOrBlank()) { "Environment variable TEST_ACCESS_TOKEN must be set to test authentication" }

      idea {
        openDuoSettings()

        duoSettings {
          authenticate(gitlabHost, token)
        }

        assert(jLabel("Success! The server host and Personal Access Token are correctly setup.").isVisible())

        button("OK").click()

        step("Check the status icon") {
          idea { assert(isDuoEnabled()) }
        }
      }
    }
  }
}
