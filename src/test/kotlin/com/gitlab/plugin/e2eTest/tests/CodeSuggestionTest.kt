package com.gitlab.plugin.e2eTest.tests

import com.automation.remarks.junit5.Video
import com.gitlab.plugin.codesuggestions.DEBOUNCE_DELAY
import com.gitlab.plugin.e2eTest.pages.idea
import com.gitlab.plugin.e2eTest.steps.EditorSteps
import com.gitlab.plugin.e2eTest.steps.ProjectSteps
import com.gitlab.plugin.e2eTest.utils.RemoteRobotExtension
import com.gitlab.plugin.e2eTest.utils.StepsLogger
import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.keyboard
import com.intellij.remoterobot.utils.waitForIgnoringError
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import java.awt.event.KeyEvent
import java.time.Duration.ofMinutes

@Suppress("ClassName")
@ExtendWith(RemoteRobotExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CodeSuggestionTest {

  init {
    StepsLogger.init()
  }

  @BeforeAll
  fun waitForIde(remoteRobot: RemoteRobot) {
    waitForIgnoringError(ofMinutes(3)) { remoteRobot.callJs("true") }
    ProjectSteps(remoteRobot).createNewProject()
    remoteRobot.idea {
      step("Ensure GitLab Duo is enabled") {
        assert(isDuoEnabled())
      }
    }
  }

  @AfterAll
  fun closeProject(remoteRobot: RemoteRobot) = ProjectSteps(remoteRobot).closeProject()

  @Nested
  inner class `with valid token set` {

    @Test
    @Video
    fun `GitLab Duo simple code generation in new file`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        ProjectSteps(remoteRobot).createNewFile("Java Class", "loop.java")
        EditorSteps(remoteRobot).clearContent()
      }

      val prompt = "// loop 5 times\\n"
      verifyCodeSuggestion(remoteRobot, prompt, "for")
    }

    @Test
    @Video
    fun `GitLab Duo code generation for a new class`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        ProjectSteps(remoteRobot).createNewFile("Java Class", "http.java")
        EditorSteps(remoteRobot).clearContent()
      }

      val prompt = "// create a http class which serves on port 8089 and responds with \"DUO\"\\n"
      verifyCodeSuggestion(remoteRobot, prompt, "public class http")
    }

    @Test
    @Video
    fun `GitLab Duo code completion for a line`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        ProjectSteps(remoteRobot).createNewFile("Java Class", "Vehicle.java")
        EditorSteps(remoteRobot).clearContent()
      }

      val prompt = "public class Vehicle {\\nprivate String make;\\n\\npublic String getMake() {\\n"
      verifyCodeSuggestion(remoteRobot, prompt, "return make;")
    }

    private fun verifyCodeSuggestion(remoteRobot: RemoteRobot, prompt: String, expectedText: String) =
      with(remoteRobot) {
        idea {
          EditorSteps(remoteRobot).addContentToEditor(prompt)

          // wait for debounce delay
          Thread.sleep(DEBOUNCE_DELAY.inWholeMilliseconds)

          waitForDuoSuggestion()

          val textEditor = textEditor().editor

          // after suggestion is displayed, accept suggestion
          keyboard { key(KeyEvent.VK_TAB) }

          println("Code suggestion for prompt: \"$prompt\"")
          println(textEditor.text)

          // check suggestion contains something relevant
          assert(textEditor.text.contains(expectedText, ignoreCase = true)) {
            "Expected \"$expectedText\" to be included in suggestion."
          }
        }
      }
  }
}
