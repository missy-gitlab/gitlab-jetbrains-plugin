package com.gitlab.plugin.workspace

import io.kotest.common.runBlocking

class WorkspaceSettingsBuilder(
  private var url: String = "https://gitlab.com",
  private var token: TokenSettings = TokenSettings.Keychain("my-awesome-token")
) {
  companion object {
    operator fun invoke(body: WorkspaceSettingsBuilder.() -> Unit): WorkspaceSettings {
      return WorkspaceSettingsBuilder().apply(body).build()
    }

    fun coCreate(body: suspend WorkspaceSettingsBuilder.() -> Unit): WorkspaceSettings {
      return WorkspaceSettingsBuilder()
        .apply { runBlocking { body() } }
        .build()
    }
  }

  fun url(value: String) = apply {
    this.url = value
  }

  fun token(value: String) = apply {
    this.token = TokenSettings.Keychain(value)
  }

  fun build(): WorkspaceSettings {
    return WorkspaceSettings(url, token)
  }
}
