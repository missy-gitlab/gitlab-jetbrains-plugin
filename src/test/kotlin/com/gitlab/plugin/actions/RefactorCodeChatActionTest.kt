package com.gitlab.plugin.actions

import com.gitlab.plugin.actions.chat.RefactorCodeChatAction
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class RefactorCodeChatActionTest : DescribeSpec({
  describe("RefactorCodeChatAction") {
    afterEach { clearAllMocks() }
    afterSpec { unmockkAll() }

    val mockActionEvent = mockk<AnActionEvent>(relaxed = true)
    val mockChatService = mockk<ChatService>(relaxed = true)
    val mockEditor = mockk<Editor>(relaxed = true)

    beforeEach {
      every { mockActionEvent.getData(CommonDataKeys.EDITOR) } returns mockEditor
      every { mockActionEvent.project?.service<ChatService>() } returns mockChatService
      every { mockEditor.selectionModel.hasSelection() } returns true
    }

    describe("actionPerformed") {
      it("calls processNewUserPrompt with the correct content and record type") {
        val action = RefactorCodeChatAction()
        action.actionPerformed(mockActionEvent)

        coVerify(exactly = 1) { mockChatService.processNewUserPrompt(any()) }
        coVerify {
          mockChatService.processNewUserPrompt(
            match {
              it.content == RefactorCodeChatAction.REFACTOR_CODE_CHAT_CONTENT
              it.type == ChatRecord.Type.REFACTOR_CODE
            }
          )
        }
      }
    }
  }
})
