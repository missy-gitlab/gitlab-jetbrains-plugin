package com.gitlab.plugin.services

import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.services.pat.PatStatus
import com.gitlab.plugin.util.TokenUtil
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class GitLabUserServiceTest : DescribeSpec({
  mockkObject(TokenUtil)

  val url = "https://gitlab.com"
  val token = "gl_token"

  val patStatusService = mockk<PatStatusService>()
  val userService = GitLabUserService(patStatusService)

  beforeEach {
    mockDuoContextServicePersistentSettings()

    every { DuoPersistentSettings.getInstance().url } returns url
    every { TokenUtil.getToken() } returns token
  }

  afterEach {
    clearAllMocks()
  }

  it("user id should initially be null") {
    userService.getCurrentUserId() shouldBe null
  }

  it("refreshes user id") {
    coEvery { patStatusService.currentStatus(url, token) } returns patForUser(1)
    userService.invalidateAndFetchCurrentUser()
    userService.getCurrentUserId() shouldBe 1

    coEvery { patStatusService.currentStatus(url, token) } returns patForUser(2)
    userService.invalidateAndFetchCurrentUser()
    userService.getCurrentUserId() shouldBe 2
  }
})

private fun patForUser(id: Int): PatStatus.Accepted {
  return mockk {
    every { userId } returns id
    every { isValid } returns true
  }
}
