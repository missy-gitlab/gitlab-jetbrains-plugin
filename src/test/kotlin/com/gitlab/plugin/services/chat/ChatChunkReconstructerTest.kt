package com.gitlab.plugin.services.chat

import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.type.AiMessageRole
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe

class ChatChunkReconstructerTest : DescribeSpec({
  fun createChunk(
    chunkId: Int,
    content: String
  ): ChatSubscription.AiCompletionResponse = ChatSubscription.AiCompletionResponse(
    chunkId = chunkId,
    content = content,
    contentHtml = "<p>$content</p>",
    errors = emptyList(),
    extras = null,
    id = "123",
    requestId = "123",
    role = AiMessageRole.ASSISTANT,
    timestamp = "",
    type = null,
  )

  describe("ChatChunkReconstructer") {
    describe("reconstructContent") {
      it("combines the chunks up to the first missed chunk") {
        val chunk1 = createChunk(1, "chunk 1")
        val chunk2 = createChunk(2, "chunk 2")
        val chunk3 = createChunk(3, "chunk 3")
        // chunk4 is missing
        val chunk5 = createChunk(5, "chunk 5")
        val chunk6 = createChunk(6, "chunk 6")

        val chunks: Map<Int, ChatSubscription.AiCompletionResponse> = mapOf(
          1 to chunk1,
          2 to chunk2,
          3 to chunk3,
          5 to chunk5,
          6 to chunk6
        )

        val result = ChatChunkReconstructer.reconstructContent(chunks)

        result.first.shouldBe("chunk 1chunk 2chunk 3")
        result.second.shouldBe("<p>chunk 1</p><p>chunk 2</p><p>chunk 3</p>")
      }

      it("is empty when there are no chunks") {
        val chunks: Map<Int, ChatSubscription.AiCompletionResponse> = emptyMap()
        val result = ChatChunkReconstructer.reconstructContent(chunks)
        result.first.shouldBe("")
        result.second.shouldBe("")
      }

      it("is empty when there is no chunkId: 1") {
        val chunk2 = createChunk(2, "chunk 2")
        val chunks: Map<Int, ChatSubscription.AiCompletionResponse> = mapOf(
          2 to chunk2
        )
        val result = ChatChunkReconstructer.reconstructContent(chunks)
        result.first.shouldBe("")
        result.second.shouldBe("")
      }
    }
  }
})
