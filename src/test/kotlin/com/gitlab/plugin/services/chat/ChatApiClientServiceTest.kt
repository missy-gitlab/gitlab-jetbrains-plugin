package com.gitlab.plugin.services.chat

import com.apollographql.apollo3.api.ApolloResponse
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.chat.api.model.AiAction
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.graphql.type.AiMessageRole
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.services.ProjectContextService
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.flow.flowOf

class ChatApiClientServiceTest : DescribeSpec({
  val userId = 999

  val userService = mockk<GitLabUserService>()
  val service = ChatApiClientService(userService)

  beforeContainer {
    mockkConstructor(GraphQLApi::class)
    mockkConstructor(ApolloClientFactory::class)
    mockkObject(ProjectContextService)

    every { userService.getCurrentUserId() } returns userId
  }

  afterContainer {
    unmockkAll()
  }

  describe("ChatApiClientService") {
    describe("processNewUserPrompt") {

      it("calls the chat mutation graphql api") {
        val ctx = ChatRecordContext(
          currentFile = ChatRecordFileContext(fileName = "file.txt", selectedText = "some text")
        )
        coEvery {
          ProjectContextService.instance.graphQLApi.chatMutation("random question", "123", ctx)
        } returns AiAction("abc", emptyList())

        val response = service.processNewUserPrompt("123", "random question", ctx)

        response.shouldNotBeNull()
        response.aiAction.errors shouldBe emptyList()
        response.aiAction.requestId shouldBe "abc"
      }

      it("returns null when the api does not return anything") {
        coEvery {
          ProjectContextService.instance.graphQLApi.chatMutation("random question", "123")
        } returns null

        val response = service.processNewUserPrompt("123", "random question")

        response shouldBe null
      }
    }

    describe("subscribeToUpdates") {
      val subscriptionId = "123"
      val requestId = "abc"

      beforeEach {
        coEvery {
          ProjectContextService.instance.graphQLApi.chatMutation("random question", subscriptionId)
        } returns AiAction(requestId, emptyList())
      }

      fun createChunk(chunkId: Int?, content: String): ChatSubscription.AiCompletionResponse = ChatSubscription.AiCompletionResponse(
        chunkId = chunkId,
        content = content,
        contentHtml = "<p>$content</p>",
        errors = emptyList(),
        extras = null,
        id = "123",
        requestId = "123",
        role = AiMessageRole.ASSISTANT,
        timestamp = "",
        type = null,
      )

      it("calls onMessageReceived for each message sent back from the stream") {
        val chunk1 = ChatSubscription.Data(aiCompletionResponse = createChunk(1, "chunk 1"))
        val chunk2 = ChatSubscription.Data(aiCompletionResponse = createChunk(2, "chunk 2"))

        val apolloResponse1 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk1
        ).build()

        val apolloResponse2 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk2
        ).build()

        coEvery {
          ProjectContextService.instance.graphQLApi.Chat.subscription(clientSubscriptionId = subscriptionId, userId = UserID("gid://gitlab/User/999"))
        } returns flowOf(
          apolloResponse1,
          apolloResponse2
        )

        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        service.subscribeToUpdates(subscriptionId, mockOnMessageReceived)

        coVerify(exactly = 2) { mockOnMessageReceived(any()) }
      }

      it("does not call onMessageReceived if an empty response is received") {
        val apolloResponse = ApolloResponse.Builder<ChatSubscription.Data>(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = null
        ).build()

        coEvery {
          ProjectContextService.instance.graphQLApi.Chat.subscription(clientSubscriptionId = subscriptionId, userId = UserID("gid://gitlab/User/999"))
        } returns flowOf(
          apolloResponse
        )

        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        service.subscribeToUpdates(subscriptionId, mockOnMessageReceived)

        coVerify(exactly = 0) { mockOnMessageReceived(any()) }
      }

      it("throws when userId is not found it fails") {
        every { userService.getCurrentUserId() } returns null

        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        shouldThrow<CurrentUserNotFoundException> { service.subscribeToUpdates(subscriptionId, mockOnMessageReceived) }
      }
    }
  }
})
