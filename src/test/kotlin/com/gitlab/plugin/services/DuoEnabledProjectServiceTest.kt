package com.gitlab.plugin.services

import com.apollographql.apollo3.ApolloClient
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.chat.exceptions.GitLabGraphQLResponseException
import com.gitlab.plugin.graphql.ProjectQuery
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.mockk.*

class DuoEnabledProjectServiceTest : DescribeSpec({
  beforeContainer {
    mockkObject(ProjectContextService)
    mockkConstructor(GraphQLApi::class)
    mockkConstructor(ApolloClientFactory::class)
  }

  afterContainer {
    unmockkAll()
  }

  describe("DuoEnabledProjectService") {
    val mockApolloClient = mockk<ApolloClient>()
    val mockGitLabProjectService = mockk<GitLabProjectService>(relaxed = true)
    val projectPath = "test/test-project"
    val project = mockk<Project>()

    beforeEach {
      clearMocks(mockApolloClient)
      clearMocks(project)
      DuoEnabledProjectService.duoEnabledProjectCache.clear()
      every { anyConstructed<ApolloClientFactory>().create() } returns mockApolloClient
      every { project.service<GitLabProjectService>() } returns mockGitLabProjectService
      every { mockGitLabProjectService.getCurrentProjectPath() } returns projectPath
      every { project.name } returns "test-name"
    }

    describe("isDuoEnabledForProject") {
      it("returns false for a project with duo disabled") {
        coEvery {
          ProjectContextService.instance.graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(false)
        val service = DuoEnabledProjectService(project)
        val response = service.isDuoEnabledForProject()
        response.shouldBeFalse()
      }

      it("returns true for a project with duo enabled") {
        coEvery {
          ProjectContextService.instance.graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(true)
        val service = DuoEnabledProjectService(project)
        val response = service.isDuoEnabledForProject()
        response.shouldBeTrue()
      }

      it("returns true when given an empty project path") {
        coEvery {
          ProjectContextService.instance.graphQLApi.getProject("")
        } returns ProjectQuery.Project(true)
        every { mockGitLabProjectService.getCurrentProjectPath() } returns ""
        val service = DuoEnabledProjectService(project)
        val response = service.isDuoEnabledForProject()
        response.shouldBeTrue()
      }

      it("returns true when project doesn't exist") {
        coEvery {
          ProjectContextService.instance.graphQLApi.getProject(projectPath)
        } returns null
        val service = DuoEnabledProjectService(project)
        val response = service.isDuoEnabledForProject()
        response.shouldBeTrue()
      }

      it("returns true when GraphQL is throwing an exception") {
        coEvery {
          ProjectContextService.instance.graphQLApi.getProject(any())
        } throws GitLabGraphQLResponseException(mockk<Throwable>())

        val service = DuoEnabledProjectService(project)
        val response = service.isDuoEnabledForProject()
        response.shouldBeTrue()
      }
    }
  }
})
