package com.gitlab.plugin.chat

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.GitLabResponseException
import com.gitlab.plugin.chat.api.abstraction.ChatApiClient
import com.gitlab.plugin.chat.api.model.AiAction
import com.gitlab.plugin.chat.api.model.AiActionResponse
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.extensions.fromSelectedEditor
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.chat.telemetry.trackFeedback
import com.gitlab.plugin.chat.view.abstraction.ChatView
import com.gitlab.plugin.chat.view.model.AppReadyMessage
import com.gitlab.plugin.chat.view.model.ChatViewMessage
import com.gitlab.plugin.chat.view.model.NewPromptMessage
import com.gitlab.plugin.chat.view.model.TrackFeedbackMessage
import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.telemetry.StandardContext
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.project.Project
import com.snowplowanalytics.snowplow.tracker.Tracker
import com.snowplowanalytics.snowplow.tracker.constants.Parameter
import com.snowplowanalytics.snowplow.tracker.events.Structured
import com.snowplowanalytics.snowplow.tracker.payload.SelfDescribingJson
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import java.util.*

@OptIn(ExperimentalCoroutinesApi::class)
class ChatControllerTest : DescribeSpec({
  lateinit var chatController: ChatController
  lateinit var chatHistory: ChatHistory
  lateinit var viewOnMessageCallback: ((message: ChatViewMessage) -> Unit)

  val api = mockk<ChatApiClient>(relaxed = true)
  val view = mockk<ChatView>(relaxed = true)
  val project = mockk<Project>(relaxed = true)
  val logger = mockk<Logger>(relaxed = true)
  val userSerivce = mockk<GitLabUserService>(relaxed = true)
  val testDispatcher = UnconfinedTestDispatcher()
  val context = mockk<ChatRecordContext>(relaxed = true)

  beforeSpec {
    mockkStatic(ChatRecordContext::fromSelectedEditor)
  }

  beforeEach {
    every { view.onMessage(captureLambda()) } answers {
      viewOnMessageCallback = lambda<(message: ChatViewMessage) -> Unit>().captured
    }

    chatHistory = ChatHistory()
    chatController = ChatController(
      chatApiClient = api,
      chatView = view,
      chatHistory = chatHistory,
      dispatcher = testDispatcher,
      project = project,
      logger = logger,
      userService = userSerivce
    )

    every { userSerivce.getCurrentUserId() } returns 123
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("init") {
    describe("chatView") {
      describe("onMessage") {
        describe("NewPromptMessage") {
          beforeEach {
            coEvery { ChatRecordContext.fromSelectedEditor(project) } returns context
          }

          it("shows the view") {
            viewOnMessageCallback(NewPromptMessage("ping"))

            verify(exactly = 1) { view.show() }
          }

          it("adds an assistant record to chat history") {
            viewOnMessageCallback(NewPromptMessage("ping"))

            verify {
              view.addRecord(
                match {
                  it.record == chatHistory.records[1] &&
                    it.record.role == ChatRecord.Role.ASSISTANT
                }
              )
            }
          }

          context("when editor context is available") {
            it("adds a user record with context to chat history") {
              viewOnMessageCallback(NewPromptMessage("ping"))

              verify {
                view.addRecord(
                  match {
                    it.record == chatHistory.records[0] &&
                      it.record.role == ChatRecord.Role.USER &&
                      it.record.context == context
                  }
                )
              }
            }
          }

          context("when editor context is not available") {
            beforeEach {
              coEvery { ChatRecordContext.fromSelectedEditor(project) } returns null
            }

            it("adds a user record without context to chat history") {
              viewOnMessageCallback(NewPromptMessage("ping"))

              verify {
                view.addRecord(
                  match {
                    it.record == chatHistory.records[0] &&
                      it.record.role == ChatRecord.Role.USER &&
                      it.record.context == null
                  }
                )
              }
            }
          }
        }

        describe("appReadyMessage") {
          it("restores history to view") {
            // arrange
            val chatRecords = listOf(
              ChatRecord(
                role = ChatRecord.Role.USER,
                state = ChatRecord.State.READY,
                requestId = UUID.randomUUID().toString(),
                content = "User message"
              ),
              ChatRecord(
                role = ChatRecord.Role.ASSISTANT,
                state = ChatRecord.State.READY,
                requestId = UUID.randomUUID().toString(),
                content = "Assistant response"
              )
            )

            chatRecords.forEach { chatHistory.addRecord(it) }

            // act
            viewOnMessageCallback.invoke(AppReadyMessage)

            // assert
            // Verify each record in chat history is added to the view
            chatRecords.forEach { record ->
              verify {
                view.addRecord(
                  match {
                    it.record == record
                  }
                )
              }
            }
          }
        }
      }
    }

    describe("process TrackFeedbackMessage") {
      mockkObject(StandardContext, GitLabApplicationService, GitLabBundle)
      mockkStatic(ApplicationInfo::getInstance)
      val tracker: Tracker = mockk()

      beforeEach {

        every { StandardContext.build(any()) } returns SelfDescribingJson(
          "standard_context",
          mapOf("extra" to "extra-val")
        )

        every { tracker.track(any()) } returns emptyList()

        every { ApplicationInfo.getInstance().build.asString() } returns "IU-232.9921.47"
        every { ApplicationInfo.getInstance().shortCompanyName } returns "JetBrains"

        every { GitLabBundle.plugin()?.name } returns "GitLab Duo"
        every { GitLabBundle.plugin()?.version } returns "0.4.1"

        every { GitLabApplicationService.getInstance().snowplowTracker } returns tracker
      }
      afterEach { unmockkAll() }

      it("tracks a message") {
        val message = slot<Structured>()
        trackFeedback(TrackFeedbackMessage("", TrackFeedbackMessage.Data("", emptyList())))

        verify(exactly = 1) { tracker.track(capture(message)) }
        message.captured.payload.map[Parameter.SE_CATEGORY] shouldBe "ask_gitlab_chat"
        message.captured.payload.map[Parameter.SE_LABEL] shouldBe "response_feedback"
        message.captured.payload.map[Parameter.SE_ACTION] shouldBe "click_button"
        message.captured.payload.map[Parameter.SE_PROPERTY] shouldBe null
        message.captured.context.size shouldBe 2
      }
    }

    describe("processNewUserPrompt") {
      describe("with GENERAL type and valid input") {
        it("add user and assistant record") {
          // arrange
          val content = "ping"
          val requestId = "00000000-00000000-00000000-00000000"

          coEvery {
            api.processNewUserPrompt(any(), any(), any())
          } returns AiActionResponse(AiAction(requestId, emptyList()))

          // act
          chatController.processNewUserPrompt(NewUserPromptRequest(content))

          // assert

          // assert chat history
          chatHistory.records shouldHaveSize 2

          chatHistory.records[0].let {
            it.role shouldBe ChatRecord.Role.USER
            it.content shouldBe content
          }

          chatHistory.records[1].role shouldBe ChatRecord.Role.ASSISTANT

          val subscriptionId = chatHistory.records[0].id

          // assert view
          verify(exactly = 1) { view.show() }
          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[0]
              }
            )
          }

          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[1]
              }
            )
          }

          // assert api client
          coVerify { api.processNewUserPrompt(subscriptionId, content) }
          coVerify { api.subscribeToUpdates(subscriptionId, any()) }
        }

        it("sets up subscription and handles updates correctly") {
          // arrange
          val content = "ping"
          val requestId = UUID.randomUUID().toString()
          val aiActionResponse = AiActionResponse(AiAction(requestId, emptyList()))
          val newUserPromptRequest = NewUserPromptRequest(content)

          coEvery { api.processNewUserPrompt(any(), any(), any()) } returns aiActionResponse

          val assistantContent = "pong"
          val aiMessage =
            AiMessage(
              requestId,
              role = "assistant",
              content = assistantContent,
              contentHtml = "",
              timestamp = ""
            )

          coEvery { api.subscribeToUpdates(any(), any()) } coAnswers {
            secondArg<suspend (message: AiMessage) -> Unit>().invoke(aiMessage)
          }

          // act
          chatController.processNewUserPrompt(newUserPromptRequest)

          // assert
          // Verify that subscribeToUpdates is called with correct parameters
          coVerify { api.subscribeToUpdates(any(), any()) }

          // Verify that the chat history and view are updated with the new information from the subscription
          chatHistory.records.last().let {
            it.role shouldBe ChatRecord.Role.ASSISTANT
            it.content shouldBe assistantContent

            // Verify that the view is updated with the new assistant record
            verify {
              view.updateRecord(
                match { message ->
                  message.record == it
                }
              )
            }
          }
        }
      }

      describe("with NEW_CONVERSATION type") {
        it("adds user record while not adding new assistant record") {
          // arrange
          val content = "Start New Conversation"
          val requestId = "newConvRequestId"
          val newUserPromptRequest = NewUserPromptRequest(content, ChatRecord.Type.NEW_CONVERSATION)

          coEvery {
            api.processNewUserPrompt(any(), any(), any())
          } returns AiActionResponse(AiAction(requestId, emptyList()))

          // act
          chatController.processNewUserPrompt(newUserPromptRequest)

          // assert

          // Assert that a new conversation record is added to chat history
          chatHistory.records shouldHaveSize 1
          chatHistory.records[0].let {
            it.role shouldBe ChatRecord.Role.USER
            it.type shouldBe ChatRecord.Type.NEW_CONVERSATION
            it.content shouldBe content
          }

          // Assert that the view is shown and the record is added to the view
          verify(exactly = 1) { view.show() }
          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[0]
              }
            )
          }

          // Assert that no assistant record is created and no subscription to updates is made
          coVerify(exactly = 0) { api.subscribeToUpdates(any(), any()) }
        }
      }
    }

    describe("clearChat") {
      val requestId = "00000000-00000000-00000000-00000000"
      val subscriptionId = UUID.randomUUID().toString()
      describe("with successful API response") {
        it("clears history and the view") {
          coEvery {
            api.clearChat(subscriptionId)
          } returns AiActionResponse(AiAction(requestId, emptyList()))

          val initialChatRecords = listOf(
            ChatRecord(
              role = ChatRecord.Role.USER,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            ),
            ChatRecord(
              role = ChatRecord.Role.ASSISTANT,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            )
          )
          initialChatRecords.forEach { chatHistory.addRecord(it) }

          chatController.clearChatWindow(subscriptionId)

          verify(exactly = 1) { view.clear() }
          chatHistory.records shouldHaveSize 0
        }
      }

      describe("with API call failure") {
        it("logs the error and does not clear the history or view") {
          // arrange
          val responseException = mockk<GitLabResponseException>()
          coEvery { responseException.message } returns "Error Message"

          coEvery {
            api.clearChat(subscriptionId)
          } throws responseException

          val initialChatRecords = listOf(
            ChatRecord(
              role = ChatRecord.Role.USER,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            ),
            ChatRecord(
              role = ChatRecord.Role.ASSISTANT,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            )
          )
          initialChatRecords.forEach { chatHistory.addRecord(it) }

          chatController.clearChatWindow(subscriptionId)

          verify(exactly = 0) { view.clear() }
          chatHistory.records shouldBe initialChatRecords
          coVerify { logger.error(any<String>()) }
        }
      }
      describe("with API response errors") {
        it("logs the errors and does not clear history or view") {
          val aiAction = AiAction(requestId, listOf("Test error 1", "Test error 2"))

          coEvery {
            api.clearChat(subscriptionId)
          } returns AiActionResponse(aiAction)

          val initialChatRecords = listOf(
            ChatRecord(
              role = ChatRecord.Role.USER,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            ),
            ChatRecord(
              role = ChatRecord.Role.ASSISTANT,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            )
          )
          initialChatRecords.forEach { chatHistory.addRecord(it) }

          chatController.clearChatWindow(subscriptionId)

          verify(exactly = 0) { view.clear() }
          chatHistory.records shouldBe initialChatRecords
        }
      }
    }
  }
})
