package com.gitlab.plugin.api.duo.graphql

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.annotations.ApolloExperimental
import com.apollographql.apollo3.api.Optional
import com.apollographql.apollo3.testing.MapTestNetworkTransport
import com.apollographql.apollo3.testing.registerTestResponse
import com.gitlab.plugin.graphql.ChatMutation
import com.gitlab.plugin.graphql.ChatQuery
import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.graphql.type.*
import com.gitlab.plugin.graphql.type.AiMessageRole.ASSISTANT
import io.kotest.common.runBlocking
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.nulls.shouldNotBeNull
import io.mockk.clearAllMocks
import kotlinx.coroutines.flow.reduce
import java.util.UUID

@OptIn(ApolloExperimental::class)
class ChatTest : DescribeSpec({
  lateinit var subject: Chat
  val apolloClient: ApolloClient by lazy {
    ApolloClient.Builder()
      .networkTransport(MapTestNetworkTransport())
      .build()
  }

  beforeEach {
    subject = Chat(apolloClient)
  }

  afterEach {
    clearAllMocks()
  }

  describe("getMessages") {
    it("should return a list of messages") {
      val expected = ChatQuery.Messages(
        nodes = listOf(
          ChatQuery.Node(
            content = "hello world",
            contentHtml = "<p>hello world</p>",
            role = ASSISTANT,
            requestId = "xxx",
            type = AiMessageType.TOOL,
          )
        )
      )
      val requestIds = listOf("xxx")
      apolloClient.registerTestResponse(
        operation = ChatQuery(Optional.present(requestIds)),
        data = ChatQuery.Data(expected),
      )

      runBlocking { subject.getMessages(requestIds) }.let {
        it.shouldNotBeNull()
        it.shouldBeEqual(expected)
      }
    }
  }

  describe("send") {
    it("should return the expected request ID") {
      val expected = ChatMutation.Action(
        errors = emptyList(),
        requestId = "",
      )

      val chatInput = AiChatInput(
        content = "hello world",
        currentFile = Optional.present(
          AiCurrentFileInput(
            contentAboveCursor = Optional.present("fun hello() {"),
            fileName = "ChatTest.kt",
            selectedText = "println(\"hello world\")",
            contentBelowCursor = Optional.present("}")
          )
        ),
      )
      val clientSubscriptionId = UUID.randomUUID().toString()
      apolloClient.registerTestResponse(
        operation = ChatMutation(chatInput, clientSubscriptionId),
        data = ChatMutation.Data(expected),
      )

      runBlocking { subject.send(chatInput, clientSubscriptionId) }.let {
        it.shouldNotBeNull()
        it.shouldBeEqual(expected)
      }
    }
  }

  describe("subscribe") {
    it("returns a flow with the expected response") {
      val expected = ChatSubscription.AiCompletionResponse(
        id = "id",
        errors = emptyList(),
        requestId = "requestId",
        timestamp = "",
        chunkId = 1,
        content = "content plain",
        extras = ChatSubscription.Extras(emptyList()),
        role = ASSISTANT,
        type = AiMessageType.TOOL,
        contentHtml = "content html"
      )

      val clientSubscriptionId = "clientSubscriptionId"
      val userId = UserID("gid://gitlab/User/1")
      apolloClient.registerTestResponse(
        operation = ChatSubscription(
          clientSubscriptionId = clientSubscriptionId,
          userId = userId,
        ),
        data = ChatSubscription.Data(expected),
      )

      runBlocking {
        subject.subscription(clientSubscriptionId, userId)
      }
        .reduce { _, value -> value }
        .let {
          it.shouldNotBeNull()
          it.dataAssertNoErrors.aiCompletionResponse?.shouldBeEqual(expected)
        }
    }
  }
})
