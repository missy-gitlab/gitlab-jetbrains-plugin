package com.gitlab.plugin.api.duo

import com.apollographql.apollo3.mockserver.MockResponse
import com.apollographql.apollo3.mockserver.MockServer
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.chat.exceptions.GitLabGraphQLResponseException
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.common.runBlocking
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.net.URI

private val json = Json { ignoreUnknownKeys = true }

class GraphQLApiTest : DescribeSpec({
  lateinit var graphqlApi: GraphQLApi
  lateinit var mockServer: MockServer

  beforeEach {
    mockDuoContextServicePersistentSettings()
    mockServer = MockServer()

    every { DuoPersistentSettings.getInstance().url } returns mockServer.url()

    graphqlApi = GraphQLApi(ApolloClientFactory { "gl_pat_token" })
  }

  afterEach {
    runBlocking { mockServer.stop() }
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  describe("getCurrentUser") {
    @Suppress("DEPRECATION")
    it("returns a valid user") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "currentUser": {
                  "id": "gid://gitlab/User/1",
                  "duoChatAvailable": true,
                  "duoCodeSuggestionsAvailable": true,
                  "id": "gid://gitlab/User/1",
                  "ide": { "codeSuggestionsEnabled": true }
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.getCurrentUser() }

      actual.shouldNotBeNull()
      actual.duoChatAvailable.shouldNotBeNull().shouldBeTrue()
      actual.duoCodeSuggestionsAvailable.shouldNotBeNull().shouldBeTrue()
      actual.id.shouldBeEqual("gid://gitlab/User/1")
      actual.ide.let { ide ->
        ide.shouldNotBeNull()
        ide.codeSuggestionsEnabled.shouldBeTrue()
      }
    }

    @Suppress("DEPRECATION")
    it("returns null for fields absent from old schema versions") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "currentUser": {
                  "id": "gid://gitlab/User/1",
                  "ide": { "codeSuggestionsEnabled": true }
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.getCurrentUser() }

      actual.shouldNotBeNull()
      actual.duoChatAvailable.shouldBeNull()
      actual.duoCodeSuggestionsAvailable.shouldBeNull()
      actual.id.shouldBeEqual("gid://gitlab/User/1")
      actual.ide.let { ide ->
        ide.shouldNotBeNull()
        ide.codeSuggestionsEnabled.shouldBeTrue()
      }
    }
  }

  describe("chatMutation") {
    it("requests a ChatMutation and returns a request id") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "action": {
                  "requestId": "abc",
                  "errors": []
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.chatMutation("question", "123") }

      val request = mockServer.takeRequest()
      val requestBody = json.decodeFromString<GraphQlRequestBody>(request.body.utf8())
      requestBody.variables.chatInput.content shouldBe "question"

      actual.shouldNotBeNull()
      actual.requestId.shouldNotBeNull().shouldBe("abc")
    }

    it("includes context when present") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "action": {
                  "requestId": "abc",
                  "errors": []
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val currentFile = ChatRecordFileContext(
        fileName = "the-file.txt",
        selectedText = "some selected text",
        contentAboveCursor = "some content above",
        contentBelowCursor = "some content below"
      )
      val ctx = ChatRecordContext(currentFile = currentFile)
      val actual = runBlocking { graphqlApi.chatMutation("question", "123", context = ctx) }

      val request = mockServer.takeRequest()
      val requestBody = json.decodeFromString<GraphQlRequestBody>(request.body.utf8())
      requestBody.variables.chatInput.content shouldBe "question"
      requestBody.variables.chatInput.currentFile?.fileName shouldBe "the-file.txt"
      requestBody.variables.chatInput.currentFile?.selectedText shouldBe "some selected text"
      requestBody.variables.chatInput.currentFile?.contentAboveCursor shouldBe "some content above"
      requestBody.variables.chatInput.currentFile?.contentBelowCursor shouldBe "some content below"

      actual.shouldNotBeNull()
      actual.requestId.shouldNotBeNull().shouldBe("abc")
    }
  }

  describe("server errors") {
    it("should throw for a 500 internal server error") {
      mockServer.enqueue(
        MockResponse.Builder().body("Internal server error").statusCode(500).build()
      )

      shouldThrow<GitLabGraphQLResponseException> {
        runBlocking { graphqlApi.getCurrentUser() }
      }
    }
  }

  describe("getProject") {
    it("returns a project with duoFeatures disabled") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "project": {
                  "duoFeaturesEnabled": false
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.getProject("test/test-project") }

      actual.shouldNotBeNull()
      actual.duoFeaturesEnabled.shouldNotBeNull().shouldBeFalse()
    }

    it("returns a null project when projectPath is empty") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "project": null
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.getProject("") }
      actual.shouldBeNull()
    }

    it("throws an error if the queried field doesn't exist") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "errors": [
                {
                  "message": "Field 'duoFeaturesEnabled' doesn't exist on type 'Project'",
                  "locations": [
                    {
                      "line": 5,
                      "column": 5
                    }
                  ],
                  "path": [
                    "query ProjectQuery",
                    "project",
                    "duoFeaturesEnabled"
                  ],
                  "extensions": {
                    "code": "undefinedField",
                    "typeName": "Project",
                    "fieldName": "duoFeaturesEnabled"
                  }
                }
              ]
            }
            """
          )
          .statusCode(200)
          .build()
      )

      shouldThrow<GitLabGraphQLResponseException> {
        runBlocking { graphqlApi.getProject("test/test-project") }
      }
    }
    it("throws an error if the user is not authorized") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "errors": [
                {
                  "message": "Http request failed with status code `401`"
                }
              ]
            }
            """
          )
          .statusCode(401)
          .build()
      )

      shouldThrow<GitLabGraphQLResponseException> {
        runBlocking { graphqlApi.getProject("test/test-project") }
      }
    }
  }

  describe("base url changed") {
    var settings = WorkspaceSettingsBuilder {}

    beforeEach {
      runBlocking { mockServer.stop() }
      mockServer = MockServer()

      settings = WorkspaceSettingsBuilder.coCreate {
        url(mockServer.url())
        token("super_secure_token")
      }
    }

    it("is able to make queries to the new base url") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "project": {
                  "duoFeaturesEnabled": false
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )
      graphqlApi.updateClient(settings)

      val actual = runBlocking { graphqlApi.getProject("test/test-project") }

      val request = mockServer.takeRequest()
      request.headers["Authorization"] shouldBe "Bearer super_secure_token"
      request.headers["Host"] shouldBe URI(mockServer.url()).authority

      actual.shouldNotBeNull()
    }
  }
})

@Serializable
data class GraphQlRequestBody(val variables: GraphQlVariables) {
  @Serializable
  data class GraphQlVariables(val chatInput: ChatInput) {
    @Serializable
    data class ChatInput(val content: String, val currentFile: CurrentFile? = null) {
      @Serializable
      data class CurrentFile(
        val fileName: String,
        val selectedText: String? = null,
        val contentAboveCursor: String? = null,
        val contentBelowCursor: String? = null
      )
    }
  }
}
