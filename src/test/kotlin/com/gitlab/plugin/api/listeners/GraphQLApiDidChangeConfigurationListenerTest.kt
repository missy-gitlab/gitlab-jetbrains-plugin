package com.gitlab.plugin.api.listeners

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.workspace.DidChangeConfigurationListener
import com.gitlab.plugin.workspace.DidChangeConfigurationParams
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import com.intellij.testFramework.registerServiceInstance
import com.intellij.util.application
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class GraphQLApiDidChangeConfigurationListenerTest : BasePlatformTestCase() {
  private val graphQLApi = mockk<GraphQLApi>(relaxUnitFun = true)
  private lateinit var listener: GraphQLApiDidChangeConfigurationListener

  @BeforeEach
  override fun setUp() {
    super.setUp()

    listener = GraphQLApiDidChangeConfigurationListener(graphQLApi)

    // NOTE: remove once GraphQLApiDidChangeConfigurationListener is registered in plugin.xml
    application.registerServiceInstance(GitLabUserService::class.java, mockk(relaxUnitFun = true))
  }

  @Test
  fun `it updates GraphQL api base url on GitLab url change`() {
    val publisher = application.messageBus.syncPublisher(DidChangeConfigurationListener.DID_CHANGE_CONFIGURATION_TOPIC)
    val settings = WorkspaceSettingsBuilder {}

    publisher.onConfigurationChange(DidChangeConfigurationParams(settings))

    verify { graphQLApi.updateClient(settings) }
  }
}
