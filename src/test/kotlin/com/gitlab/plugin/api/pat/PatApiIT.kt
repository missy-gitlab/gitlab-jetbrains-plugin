package com.gitlab.plugin.api.pat

import com.gitlab.plugin.api.configureTestClient
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.integrationtest.IntegrationTestEnvironment
import com.gitlab.plugin.services.DuoContextService
import io.kotest.core.annotation.EnabledIf
import io.kotest.core.spec.style.DescribeSpec
import io.ktor.client.engine.okhttp.*
import io.mockk.*
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import javax.net.ssl.SSLHandshakeException

@EnabledIf(IntegrationTestEnvironment.Configured::class)
class PatApiIT : DescribeSpec({
  val env = IntegrationTestEnvironment.forSpec(
    PatApiIT::class,
    IntegrationTestEnvironment(
      personalAccessToken = System.getenv("TEST_ACCESS_TOKEN"),
      gitlabHost = System.getenv("TEST_GITLAB_PROXY_HOST") ?: "https://localhost:8443",
      kclass = PatApiIT::class,
    )
  )
  fun subject(ignoreCertificateErrors: Boolean) = PatApi(
    engine = OkHttp.create {
      configureTestClient(ignoreCertificateErrors = ignoreCertificateErrors)
    },
  )

  describe("a non-trusted certificate chain") {
    beforeEach {
      mockDuoContextServicePersistentSettings()
    }

    afterEach {
      clearAllMocks()
    }

    afterSpec {
      unmockkAll()
    }

    it("ignoring certificate errors").config(enabledIf = env.running()) {
      every { DuoContextService.instance.duoSettings.ignoreCertificateErrors } returns true
      subject(ignoreCertificateErrors = true).let {
        assertDoesNotThrow { it.patInfo(env.gitlabHost, env.personalAccessTokenProvider()) }
      }
    }

    it("without ignoring certificate errors").config(enabledIf = env.running()) {
      every { DuoContextService.instance.duoSettings.ignoreCertificateErrors } returns false
      subject(ignoreCertificateErrors = false).let {
        assertThrows<SSLHandshakeException> { it.patInfo(env.gitlabHost, env.personalAccessTokenProvider()) }
      }
    }
  }
})
