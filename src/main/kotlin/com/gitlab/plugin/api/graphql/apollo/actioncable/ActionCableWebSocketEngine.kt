package com.gitlab.plugin.api.graphql.apollo.actioncable

import com.apollographql.apollo3.annotations.ApolloDeprecatedSince
import com.apollographql.apollo3.annotations.ApolloDeprecatedSince.Version.v3_2_2
import com.apollographql.apollo3.api.http.HttpHeader
import com.apollographql.apollo3.network.ws.WebSocketConnection
import com.apollographql.apollo3.network.ws.WebSocketEngine
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.channels.Channel
import okhttp3.Headers
import okhttp3.Request
import okhttp3.WebSocket

class ActionCableWebSocketEngine(private val webSocketFactory: WebSocket.Factory) : WebSocketEngine {
  override suspend fun open(
    url: String,
    headers: List<HttpHeader>,
  ): WebSocketConnection {
    val messageChannel = Channel<String>(Channel.BUFFERED)

    val okHttpHeaders = Headers.Builder().apply {
      headers.forEach { header -> add(header.name, header.value) }
    }.build()

    val webSocketOpenResult = CompletableDeferred<Unit>()
    val webSocket = webSocketFactory.newWebSocket(
      request = Request.Builder().url(url).headers(okHttpHeaders).build(),
      listener = ActionCableWebSocketListener(messageChannel, webSocketOpenResult)
    )

    webSocketOpenResult.await()

    return ActionCableWebSocketConnection(messageChannel, webSocket)
  }

  @Deprecated(
    "Use open(String, List<HttpHeader>) instead.",
    ReplaceWith(
      "open(url, headers.map { HttpHeader(it.key, it.value })",
      "com.apollographql.apollo3.api.http.HttpHeader"
    )
  )
  @ApolloDeprecatedSince(version = v3_2_2)
  override suspend fun open(url: String, headers: Map<String, String>): WebSocketConnection =
    open(url, headers.map { HttpHeader(it.key, it.value) })
}
