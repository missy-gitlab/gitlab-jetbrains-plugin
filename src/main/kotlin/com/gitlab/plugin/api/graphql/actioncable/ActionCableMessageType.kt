package com.gitlab.plugin.api.graphql.actioncable

enum class ActionCableMessageType(val type: String?) {
  WELCOME("welcome"),
  CONFIRMATION("confirm_subscription"),
  DISCONNECT("disconnect"),
  PING("ping"),
  REJECTION("reject_subscription"),
  GITLAB_GRAPHQL_MESSAGE(null);

  companion object {
    fun from(obj: Any?): ActionCableMessageType {
      val matching = entries.firstOrNull { it.type == obj.toString() }
      return matching ?: GITLAB_GRAPHQL_MESSAGE
    }
  }
}
