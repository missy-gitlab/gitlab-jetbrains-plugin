package com.gitlab.plugin.api.graphql.apollo.actioncable

import com.apollographql.apollo3.api.ApolloRequest
import com.apollographql.apollo3.api.Operation
import com.apollographql.apollo3.api.variables
import com.apollographql.apollo3.exception.ApolloException
import com.apollographql.apollo3.exception.ApolloNetworkException
import com.apollographql.apollo3.network.ws.WebSocketConnection
import com.apollographql.apollo3.network.ws.WsProtocol
import com.gitlab.plugin.api.graphql.actioncable.ActionCableMessageType
import com.gitlab.plugin.api.graphql.actioncable.ActionCableMessageType.*
import com.gitlab.plugin.api.graphql.actioncable.ActionCableSubscription
import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.type.__CustomScalarAdapters
import com.intellij.openapi.diagnostic.logger
import kotlinx.coroutines.withTimeout

class ActionCableWsProtocol(
  private val connectionAcknowledgeTimeoutMs: Long,
  webSocketConnection: WebSocketConnection,
  listener: Listener,
) : WsProtocol(webSocketConnection, listener) {
  private val logger = logger<ActionCableWsProtocol>()
  private val actionCableSubscriptions: MutableList<ActionCableSubscription> = mutableListOf()

  override suspend fun connectionInit() {
    withTimeout(connectionAcknowledgeTimeoutMs) {
      val messageMap = receiveMessageMap()
      handleServerMessage(messageMap)
    }
  }

  @Suppress("UNCHECKED_CAST")
  override fun handleServerMessage(messageMap: Map<String, Any?>) {
    logger.debug("[ActionCable] Handling server message: $messageMap")
    val actionCableSubscription = actionCableSubscription(messageMap)
    val type = messageMap["type"]?.toString()
      .let { ActionCableMessageType.from(it) }
    when (type) {
      WELCOME, CONFIRMATION, PING -> Unit
      GITLAB_GRAPHQL_MESSAGE ->
        actionCableSubscription?.handleGitLabGraphQLMessage(messageMap["message"] as? Map<String, Any?>)

      DISCONNECT, REJECTION -> {
        if (actionCableSubscription == null) {
          listener.networkError(ApolloNetworkException("Server requested disconnect"))
        } else {
          unsubscribeFrom(actionCableSubscription)
          listener.operationError(actionCableSubscription.requestUuid, null)
        }
      }
    }
  }

  override fun <D : Operation.Data> startOperation(request: ApolloRequest<D>) {
    if (request.operation !is ChatSubscription) {
      throw ApolloException(
        message = "[requestUuid: ${request.requestUuid}] Unable to start unknown operation ${request.operation.name()}"
      )
    }

    val actionCableSubscription = ActionCableSubscription(
      listener = listener,
      identifier = composeIdentifier(request.operation).toUtf8(),
      requestUuid = request.requestUuid.toString(),
    )
    actionCableSubscriptions.add(actionCableSubscription)
    sendMessageMapText(actionCableSubscription.command(ActionCableSubscription.CommandType.SUBSCRIBE))
  }

  override fun <D : Operation.Data> stopOperation(request: ApolloRequest<D>) {
    val actionCableSubscription = actionCableSubscriptions.find {
      it.requestUuid == request.requestUuid.toString()
    } ?: return

    unsubscribeFrom(actionCableSubscription)
  }

  private fun actionCableSubscription(actionCableMessage: Map<String, Any?>?) =
    actionCableMessage?.get("identifier")
      ?.let { it as? String }
      ?.let { id -> actionCableSubscriptions.find { sub -> id == sub.identifier } }

  private fun <D : Operation.Data> composeIdentifier(operation: Operation<D>) = mapOf(
    "channel" to "GraphqlChannel",
    "operationName" to operation.name(),
    "query" to operation.document(),
    "variables" to variables(operation),
  )

  private fun unsubscribeFrom(actionCableSubscription: ActionCableSubscription) {
    actionCableSubscriptions.remove(actionCableSubscription)
    sendMessageMapText(actionCableSubscription.command(ActionCableSubscription.CommandType.UNSUBSCRIBE))
  }

  private fun <D : Operation.Data> variables(operation: Operation<D>) = operation
    .variables(__CustomScalarAdapters)
    .valueMap
    .toMutableMap()
}
