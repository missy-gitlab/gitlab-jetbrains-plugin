package com.gitlab.plugin.api.graphql

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.network.http.DefaultHttpEngine
import com.apollographql.apollo3.network.ws.WebSocketNetworkTransport
import com.gitlab.plugin.api.ProxyManager
import com.gitlab.plugin.api.asProxyInfo
import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.api.duo.configureSslSocketFactory
import com.gitlab.plugin.api.graphql.apollo.actioncable.ActionCableWebSocketEngine
import com.gitlab.plugin.api.graphql.apollo.actioncable.ActionCableWsProtocolFactory
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.workspace.TokenSettings
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.intellij.collaboration.util.resolveRelative
import com.intellij.util.net.HttpConfigurable
import io.ktor.http.HttpHeaders.Authorization
import io.ktor.http.HttpHeaders.Origin
import okhttp3.OkHttpClient
import java.net.URI

class ApolloClientFactory(
  private val tokenProvider: DuoClient.TokenProvider
) {

  private val okHttpClient: OkHttpClient by lazy {
    OkHttpClient.Builder().apply {
      configureSslSocketFactory()
      followRedirects(true)

      val proxyInfo = HttpConfigurable.getInstance().asProxyInfo()
      proxyInfo?.let {
        ProxyManager(it) { url -> HttpConfigurable.getInstance().isHttpProxyEnabledForUrl(url) }.let { proxyManager ->
          proxySelector(proxyManager)
          proxyAuthenticator(proxyManager)
        }
      }
    }.build()
  }

  fun create(settings: WorkspaceSettings? = null): ApolloClient {
    val instanceUri = URI(settings?.url ?: DuoPersistentSettings.getInstance().url)

    val serverUrl: String = instanceUri
      .resolveRelative("/api/graphql")
      .toString()

    val subscriptionUrl: String = instanceUri
      .resolveRelative("/-/cable")
      .toString()
      .replace("https://", "wss://")
      .replace("http://", "ws://")

    return ApolloClient.Builder()
      .addHttpHeader(Authorization, computeBearerToken(settings))
      .httpEngine(DefaultHttpEngine(okHttpClient))
      .serverUrl(serverUrl)
      .subscriptionNetworkTransport(
        WebSocketNetworkTransport.Builder()
          .serverUrl(subscriptionUrl)
          .addHeader(Origin, instanceUri.toString())
          .addHeader(Authorization, computeBearerToken(settings))
          .webSocketEngine(ActionCableWebSocketEngine(okHttpClient))
          .protocol(ActionCableWsProtocolFactory())
          .build()
      )
      .build()
  }

  private fun computeBearerToken(settings: WorkspaceSettings?): String {
    val token = when (val tokenSettings = settings?.token) {
      is TokenSettings.Keychain -> tokenSettings.value
      else -> tokenProvider.token()
    }

    return "Bearer $token"
  }
}
