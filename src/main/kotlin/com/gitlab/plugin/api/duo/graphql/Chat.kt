package com.gitlab.plugin.api.duo.graphql

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Optional
import com.apollographql.apollo3.exception.ApolloException
import com.gitlab.plugin.chat.exceptions.GitLabGraphQLResponseException
import com.gitlab.plugin.chat.exceptions.UnsupportedGitLabVersionException
import com.gitlab.plugin.graphql.ChatMutation
import com.gitlab.plugin.graphql.ChatQuery
import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.graphql.type.AiChatInput

class Chat(private val apolloClient: ApolloClient) {
  @Suppress("DEPRECATION")
  @Throws(GitLabGraphQLResponseException::class, UnsupportedGitLabVersionException::class)
  suspend fun getMessages(requestIds: List<String>?): ChatQuery.Messages? = try {
    val query = ChatQuery(Optional.present(requestIds))
    apolloClient.query(query)
      .execute()
      .dataAssertNoErrors
      .messages
  } catch (cause: ApolloException) {
    throw GitLabGraphQLResponseException(cause)
  }

  @Suppress("DEPRECATION")
  @Throws(GitLabGraphQLResponseException::class, UnsupportedGitLabVersionException::class)
  suspend fun send(chatInput: AiChatInput, clientSubscriptionId: String): ChatMutation.Action? = try {
    val mutation = ChatMutation(chatInput, clientSubscriptionId)
    apolloClient.mutation(mutation)
      .execute()
      .dataAssertNoErrors
      .action
  } catch (cause: ApolloException) {
    throw GitLabGraphQLResponseException(cause)
  }

  @Throws(GitLabGraphQLResponseException::class, UnsupportedGitLabVersionException::class)
  fun subscription(clientSubscriptionId: String, userId: UserID) = try {
    apolloClient.subscription(
      ChatSubscription(
        clientSubscriptionId = clientSubscriptionId,
        userId = userId,
      )
    ).toFlow()
  } catch (cause: ApolloException) {
    throw GitLabGraphQLResponseException(cause)
  }
}
