package com.gitlab.plugin.api.duo

import com.apollographql.apollo3.api.Optional
import com.apollographql.apollo3.exception.ApolloException
import com.gitlab.plugin.api.duo.graphql.Chat
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.chat.api.model.AiAction
import com.gitlab.plugin.chat.exceptions.GitLabGraphQLResponseException
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.graphql.ChatMutation
import com.gitlab.plugin.graphql.CurrentUserQuery
import com.gitlab.plugin.graphql.ProjectQuery
import com.gitlab.plugin.graphql.type.AiChatInput
import com.gitlab.plugin.graphql.type.AiCurrentFileInput
import com.gitlab.plugin.workspace.WorkspaceSettings
import java.util.*

class GraphQLApi(private val apolloClientFactory: ApolloClientFactory) {
  private var apolloClient = apolloClientFactory.create()

  @Suppress("detekt:VariableNaming", "PropertyName")
  val Chat: Chat
    get() = Chat(apolloClient)

  @Throws(GitLabGraphQLResponseException::class)
  suspend fun getCurrentUser() = try {
    apolloClient.query(CurrentUserQuery())
      .execute()
      .dataAssertNoErrors
      .currentUser
  } catch (cause: ApolloException) {
    throw GitLabGraphQLResponseException(cause)
  }

  @Suppress("DEPRECATION")
  suspend fun chatMutation(
    content: String,
    clientSubscriptionId: String,
    context: ChatRecordContext? = null
  ): AiAction? = try {
    val currentFile = Optional.presentIfNotNull(
      context?.let {
        extractCurrentFile(it.currentFile)
      }
    )
    val request = ChatMutation(
      AiChatInput(
        content = content,
        currentFile = currentFile
      ),
      clientSubscriptionId
    )

    val response = apolloClient.mutation(request)
      .execute()
      .dataAssertNoErrors
      .action

    response?.requestId?.let { AiAction(requestId = it, errors = response.errors) }
  } catch (cause: ApolloException) {
    throw GitLabGraphQLResponseException(cause)
  }

  private fun extractCurrentFile(currentFile: ChatRecordFileContext) = AiCurrentFileInput(
    fileName = currentFile.fileName,
    selectedText = currentFile.selectedText,
    contentAboveCursor = Optional.present(currentFile.contentAboveCursor),
    contentBelowCursor = Optional.present(currentFile.contentBelowCursor)
  )

  @Throws(GitLabGraphQLResponseException::class)
  suspend fun getProject(projectPath: String) = try {
    apolloClient.query(ProjectQuery(projectPath))
      .execute()
      .dataAssertNoErrors
      .project
  } catch (cause: ApolloException) {
    throw GitLabGraphQLResponseException(cause)
  }

  fun updateClient(settings: WorkspaceSettings) {
    apolloClient = apolloClientFactory.create(settings)
  }
}
