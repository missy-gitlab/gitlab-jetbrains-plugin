package com.gitlab.plugin.api.duo.requests

class Completion {
  data class Payload(
    val promptVersion: Int = 1,
    val projectPath: String = "",
    val projectId: Int = -1,
    val currentFile: CodeSuggestionRequestFile = CodeSuggestionRequestFile(),
    val telemetry: List<Telemetry> = emptyList()
  ) {
    data class CodeSuggestionRequestFile(
      val fileName: String = "",
      val contentAboveCursor: String = "",
      val contentBelowCursor: String = ""
    )

    data class Telemetry(
      val modelEngine: String,
      val modelName: String,
      val lang: String,
      val errors: Int
    )
  }

  data class Response(
    val choices: List<Choice>,
    val model: Model
  ) {
    data class Choice(
      val text: String
    )

    data class Model(
      val engine: String = "",
      val name: String = "",
      val lang: String = ""
    )

    val firstSuggestion: String
      get() = choices.first().text

    fun hasSuggestions() = choices.isNotEmpty() && choices.all { it.text.isNotBlank() }
  }
}
