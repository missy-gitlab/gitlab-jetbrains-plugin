package com.gitlab.plugin.workspace

data class WorkspaceSettings(val url: String, val token: TokenSettings)

sealed interface TokenSettings {
  data class Keychain(val value: String) : TokenSettings
}
