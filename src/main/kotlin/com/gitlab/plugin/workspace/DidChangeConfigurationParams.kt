package com.gitlab.plugin.workspace

data class DidChangeConfigurationParams(val settings: WorkspaceSettings)
