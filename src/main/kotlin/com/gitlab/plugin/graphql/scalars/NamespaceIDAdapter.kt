package com.gitlab.plugin.graphql.scalars

class NamespaceID(gid: String) : GitLabScalar(gid)

val NamespaceIDAdapter =
  GitLabScalarAdapter { NamespaceID(it) }
