package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.*
import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.services.PatStatusService
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.ui.components.BrowserLink
import com.intellij.ui.dsl.builder.Cell
import com.intellij.util.application
import io.ktor.http.*
import kotlinx.coroutines.runBlocking
import javax.swing.JLabel

fun verifyServerConfiguration(
  host: String,
  token: String,
  updatableLabel: JLabel,
  errorReportingInstructions: Cell<BrowserLink>
) {
  errorReportingInstructions.visible(false)

  runBlocking {
    var errorMessage = ""
    val client = DuoContextService.instance.duoHttpClient.copy(
      host = host,
      tokenProvider = { token },
      shouldRetry = false,
      exceptionHandler = { exception ->
        errorMessage = when (exception) {
          is GitLabOfflineException -> GitLabBundle.message("settings.ui.gitlab.verify-setup.unreachable")
          is GitLabForbiddenException -> GitLabBundle.message("settings.ui.gitlab.verify-setup.forbidden")
          is GitLabUnauthorizedException -> GitLabBundle.message("settings.ui.gitlab.verify-setup.unauthorized")
          is GitLabProxyException -> GitLabBundle.message("settings.ui.gitlab.verify-setup.proxy-exception")
          else -> {
            if (exception.isStatusNotFound() && host.isGitLabCom()) {
              GitLabBundle.message("code-suggestions.not-licensed")
            } else {
              Logger.getInstance(this::class.java).info(exception)
              errorReportingInstructions.visible(true)
              GitLabBundle.message("settings.ui.gitlab.verify-setup.unexpected-error")
            }
          }
        }
      }
    )

    val verified = DuoApi(client, duoSettings = DuoPersistentSettings()).verify()

    if (verified) {
      val patStatus = application.service<PatStatusService>().currentStatus(host = host, token = token)

      if (!patStatus.isValid) {
        updatableLabel.text = patStatus.message()
      } else {
        updatableLabel.text = GitLabBundle.message("settings.ui.gitlab.verify-setup.success")
      }
    } else {
      val errorMessageHTML = "<html><body style='width: 350px;'>%s</body></html>"
      updatableLabel.text = String.format(errorMessageHTML, errorMessage)
    }
  }
}

private fun Throwable.isStatusNotFound() =
  this is GitLabResponseException && response.status == HttpStatusCode.NotFound

private fun String.isGitLabCom() = lowercase() == "${GitLabUtil.GITLAB_DEFAULT_URL}/"
