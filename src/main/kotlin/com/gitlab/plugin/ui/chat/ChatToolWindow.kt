package com.gitlab.plugin.ui.chat

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory

internal class ChatToolWindow : ToolWindowFactory {
  override suspend fun isApplicableAsync(project: Project): Boolean =
    DuoPersistentSettings.getInstance().duoChatEnabled

  override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
    val factory = toolWindow.contentManager.factory
    val chatService = project.service<ChatService>()
    val chatBrowserComponent = chatService.browserComponent

    val content =
      factory
        .createContent(chatBrowserComponent, null, false)
        .apply { setDisposer(chatService) }

    toolWindow.contentManager.addContent(content)
  }
}
