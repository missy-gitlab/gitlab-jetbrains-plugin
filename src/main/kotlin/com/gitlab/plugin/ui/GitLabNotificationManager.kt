package com.gitlab.plugin.ui

import com.intellij.notification.NotificationGroup
import com.intellij.notification.NotificationGroupManager
import com.intellij.notification.NotificationType
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project

const val GENERAL_NOTIFICATION_GROUP_ID = "com.gitlab.plugin.notification.general"
const val IMPORTANT_NOTIFICATION_GROUP_ID = "com.gitlab.plugin.notification.important"

class GitLabNotificationManager {
  fun sendNotification(notificationData: Notification, project: Project? = null) {
    val manager = getGeneralNotificationGroup()

    val nativeNotification = manager.createNotification(
      notificationData.title,
      notificationData.message,
      NotificationType.INFORMATION
    )
    nativeNotification.setIcon(GitLabIcons.NotificationIcon)

    notificationData.actions.forEach { action ->
      nativeNotification.addAction(
        DumbAwareAction.create(action.title) {
          action.run {
            nativeNotification.expire()
          }
        }
      )
    }

    nativeNotification.notify(project)
  }

  private fun getGeneralNotificationGroup(): NotificationGroup =
    NotificationGroupManager.getInstance().getNotificationGroup(GENERAL_NOTIFICATION_GROUP_ID)

  private fun getImportantNotificationGroup(): NotificationGroup =
    NotificationGroupManager.getInstance().getNotificationGroup(IMPORTANT_NOTIFICATION_GROUP_ID)
}
