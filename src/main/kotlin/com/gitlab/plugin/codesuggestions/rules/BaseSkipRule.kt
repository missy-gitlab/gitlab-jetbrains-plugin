package com.gitlab.plugin.codesuggestions.rules

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.diagnostic.Logger

abstract class BaseSkipRule(private val logger: Logger) {
  private val logPrefix = "Skipping suggestion"

  abstract fun shouldSkipSuggestion(request: InlineCompletionRequest): Boolean

  protected open fun log(message: String) =
    logger.info("$logPrefix: $message")
}
