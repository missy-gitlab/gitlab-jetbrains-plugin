package com.gitlab.plugin.codesuggestions.rules

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.diagnostic.Logger

/**
Suggestions should not be provided when there are less than 10 characters before the cursor.
 */
class PrefixTooShort(logger: Logger = Logger.getInstance(PrefixTooShort::class.java)) :
  BaseSkipRule(logger) {
  companion object {
    const val MIN_CHARS = 10
  }

  override fun shouldSkipSuggestion(request: InlineCompletionRequest): Boolean {
    if (request.document.charsSequence.take(request.endOffset).toString().length > MIN_CHARS) return false

    log("less than $MIN_CHARS characters before the cursor")
    return true
  }
}
