package com.gitlab.plugin.chat.telemetry

import com.gitlab.plugin.chat.view.model.TrackFeedbackMessage
import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.telemetry.SnowplowTracker
import com.gitlab.plugin.telemetry.StandardContext
import com.snowplowanalytics.snowplow.tracker.events.Structured

fun trackFeedback(message: TrackFeedbackMessage) {
  val tracker = GitLabApplicationService.getInstance().snowplowTracker

  with(Structured.builder()) {
    category("ask_gitlab_chat")
    action("click_button")
    label("response_feedback")
    property(message.data.feedbackChoices.joinToString(","))
    customContext(
      listOf(
        StandardContext.build(
          extra = mapOf(
            "extended_feedback" to message.data.extendedTextFeedback
          ),
        ),
        SnowplowTracker.IDE_EXTENSION_VERSION_CONTEXT
      )
    )
    build()
  }.let {
    tracker.track(it)
  }
}
