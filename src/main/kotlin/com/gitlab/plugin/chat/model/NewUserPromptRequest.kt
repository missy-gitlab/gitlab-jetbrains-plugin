package com.gitlab.plugin.chat.model

data class NewUserPromptRequest(
  val content: String,
  val type: ChatRecord.Type,
  val context: ChatRecordContext? = null
) {
  constructor(content: String) : this(content, ChatRecord.Type.fromContent(content))
  constructor(content: String, context: ChatRecordContext?) : this(
    content,
    ChatRecord.Type.fromContent(content),
    context
  )
}
