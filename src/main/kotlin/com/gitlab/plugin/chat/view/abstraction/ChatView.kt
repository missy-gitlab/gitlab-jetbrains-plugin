package com.gitlab.plugin.chat.view.abstraction

import com.gitlab.plugin.chat.view.model.ChatViewMessage
import com.gitlab.plugin.chat.view.model.NewRecordMessage
import com.gitlab.plugin.chat.view.model.UpdateRecordMessage

/**
 * Represents a chat view
 */
interface ChatView {
  /**
   * Displays the chat view to the user.
   */
  fun show()

  /**
   * Clean existing chat records.
   */
  fun clear()

  /**
   * Updates an existing chat record.
   *
   * @param message The [UpdateRecordMessage] containing the updated record
   */
  fun updateRecord(message: UpdateRecordMessage)

  /**
   * Adds a new chat record to the chat view
   *
   * @param message The [NewRecordMessage] containing the new record
   */
  fun addRecord(message: NewRecordMessage)

  /**
   * Registers a callback to be invoked when a new message is received from the chat view.
   *
   * @param block A lambda function that takes a [ChatViewMessage] as a parameter and
   * handles the incoming chat message.
   */
  fun onMessage(block: (message: ChatViewMessage) -> Unit)
}
