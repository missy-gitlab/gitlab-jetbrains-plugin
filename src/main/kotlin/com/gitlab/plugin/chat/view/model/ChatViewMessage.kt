package com.gitlab.plugin.chat.view.model

import com.gitlab.plugin.chat.model.ChatRecord
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

private const val EVENT_TYPE_KEY = "eventType"

@Serializable
sealed class ChatViewMessage {
  companion object {
    // Event types
    const val NEW_RECORD = "newRecord"
    const val UPDATE_RECORD = "updateRecord"
    const val NEW_PROMPT = "newPrompt"
    const val TRACK_FEEDBACK = "trackFeedback"
    const val APP_READY = "appReady"
    const val CLEAR = "clear"

    private val format = Json {
      classDiscriminator = EVENT_TYPE_KEY
      encodeDefaults = true
      explicitNulls = false
    }

    fun fromJson(json: String): ChatViewMessage = format.decodeFromString(serializer(), json)
  }

  fun toJson(): String = format.encodeToString(serializer(), this)
}

/**
 * Message indicating that a new prompt has been submitted by the user
 * @property content The content of the new prompt.
 */
@Serializable
@SerialName(ChatViewMessage.NEW_PROMPT)
data class NewPromptMessage(
  val content: String
) : ChatViewMessage()

/**
 * Message indicating that the user has submitted feedback
 * @property extendedTextFeedback The text content of the feedback
 * @property feedbackChoices The feedback choices
 */
@Serializable
@SerialName(ChatViewMessage.TRACK_FEEDBACK)
data class TrackFeedbackMessage(
  val content: String,
  val data: Data
) : ChatViewMessage() {
  @Serializable
  data class Data(
    val extendedTextFeedback: String,
    val feedbackChoices: List<String>
  )
}

/**
 * Message indicating that a new chat record should be shown in the UI
 * @property record A chat record
 */
@Serializable
@SerialName(ChatViewMessage.NEW_RECORD)
data class NewRecordMessage(
  val record: ChatRecord
) : ChatViewMessage()

/**
 * Message indicating that a chat record should be updated in the UI
 * @property record A chat record
 */
@Serializable
@SerialName(ChatViewMessage.UPDATE_RECORD)
data class UpdateRecordMessage(
  val record: ChatRecord
) : ChatViewMessage()

/**
 * Message indicating that the Vue application has been initialized
 */
@Serializable
@SerialName(ChatViewMessage.APP_READY)
data object AppReadyMessage : ChatViewMessage()

/**
 * Message indicating that the chat records should be cleared in the UI
 */
@Serializable
@SerialName(ChatViewMessage.CLEAR)
data object ClearChatMessage : ChatViewMessage()
