package com.gitlab.plugin.chat.exceptions

class GitLabGraphQLResponseException(cause: Throwable) :
  RuntimeException(cause)
