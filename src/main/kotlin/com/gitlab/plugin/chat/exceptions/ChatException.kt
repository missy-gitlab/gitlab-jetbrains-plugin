package com.gitlab.plugin.chat.exceptions

class ChatException(message: String) : Exception("DuoChat Exception: $message")
