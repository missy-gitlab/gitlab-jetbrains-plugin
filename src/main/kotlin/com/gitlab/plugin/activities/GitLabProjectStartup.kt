package com.gitlab.plugin.activities

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.codesuggestions.license.CodeSuggestionsLicenseStatus
import com.gitlab.plugin.services.*
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationAction
import com.gitlab.plugin.util.TokenUtil
import com.gitlab.plugin.util.gitlabStatusDisabled
import com.gitlab.plugin.util.gitlabStatusVersionUnsupported
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.ProjectActivity
import com.intellij.util.application

internal class GitLabProjectStartup : ProjectActivity {
  override suspend fun execute(project: Project) {
    val duoEnabledProjectService = project.service<DuoEnabledProjectService>()
    val context = ProjectContextService.init(project)
    val isDuoEnabled = duoEnabledProjectService.isDuoEnabledForProject()

    val token = TokenUtil.getToken()
    val patStatus = application.service<PatStatusService>().currentStatus()
    val gitLabUserService = application.service<GitLabUserService>()

    if (patStatus.isValid) {
      gitLabUserService.invalidateAndFetchCurrentUser()
      context.codeSuggestionsLicense.validateLicense()
    }

    val notification = when {
      token.isNullOrEmpty() -> {
        Notification(
          "Get started with GitLab Duo Code Suggestions.",
          "You need to configure your GitLab credentials first.",
          listOf(NotificationAction.settings(project))
        )
      }

      !patStatus.isValid -> {
        Notification(
          GitLabBundle.message("notification.title.gitlab-duo"),
          patStatus.message(),
          listOf(NotificationAction.settings(project))
        )
      }

      !isDuoEnabled -> {
        gitlabStatusDisabled()
        Notification(
          GitLabBundle.message("notification.title.gitlab-duo"),
          GitLabBundle.message("notification.duo-disabled")
        )
      }

      !context.codeSuggestionsLicense.isLicensed -> {
        CodeSuggestionsLicenseStatus.licenseMissingNotification()
      }

      context.serverServiceWithoutCallbacks.get().isVersionUpgradeRequired -> {
        gitlabStatusVersionUnsupported()
        GitLabServerService.versionUnsupportedNotification()
      }

      else -> {
        Notification(
          "Get started with GitLab Duo Code Suggestions.",
          "GitLab Duo Code Suggestions is ready to use with this project."
        )
      }
    }

    GitLabNotificationManager().sendNotification(notification)
  }
}
