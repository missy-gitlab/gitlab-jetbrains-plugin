package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.api.duo.PatProvider
import com.gitlab.plugin.api.proxiedEngine
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.IconStatusModifier
import com.gitlab.plugin.util.GitLabUtil
import com.gitlab.plugin.util.TokenUtil

/**
 * Duo context service
 *
 * Includes everything that should be globally available or has no dependency on the active project
 *
 * @constructor Singleton
 */
class DuoContextService private constructor() {
  val duoSettings by lazy { DuoPersistentSettings.getInstance() }

  val patProvider by lazy { PatProvider() }

  val duoHttpClient by lazy {
    DuoClient(
      tokenProvider = patProvider,
      exceptionHandler = CreateNotificationExceptionHandler(GitLabNotificationManager()),
      userAgent = GitLabUtil.userAgent,
      onStatusChanged = IconStatusModifier(),
      httpClientEngine = proxiedEngine()
    )
  }

  /**
   * Return whether Duo is configured with required credentials
   *
   * @return whether duo is configured
   */
  fun isDuoConfigured(): Boolean = TokenUtil.getToken()?.isNotEmpty() == true

  /**
   * Return whether Duo is enabled
   *
   * @return whether duo is enabled
   */
  fun isDuoEnabled(): Boolean = duoSettings.enabled

  companion object {
    val instance: DuoContextService by lazy {
      DuoContextService()
    }
  }
}
