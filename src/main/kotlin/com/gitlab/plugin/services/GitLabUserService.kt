package com.gitlab.plugin.services

import com.gitlab.plugin.services.pat.PatStatus
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.Service.Level.APP
import com.intellij.openapi.components.service
import com.intellij.util.application

@Service(APP)
class GitLabUserService(
  private val patStatusService: PatStatusService = application.service<PatStatusService>()
) {
  private var userId: Int? = null

  fun getCurrentUserId(): Int? = userId

  suspend fun invalidateAndFetchCurrentUser() {
    val patStatus = patStatusService.currentStatus()

    if (patStatus is PatStatus.Accepted && patStatus.isValid) {
      userId = patStatus.userId
    }
  }
}
