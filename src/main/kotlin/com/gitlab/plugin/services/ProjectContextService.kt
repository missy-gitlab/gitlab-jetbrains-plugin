package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.api.listeners.GraphQLApiDidChangeConfigurationListener
import com.gitlab.plugin.codesuggestions.license.CodeSuggestionsLicenseStatus
import com.gitlab.plugin.codesuggestions.telemetry.LogDestination
import com.gitlab.plugin.codesuggestions.telemetry.SnowplowDestination
import com.gitlab.plugin.codesuggestions.telemetry.Telemetry
import com.gitlab.plugin.ui.CompletionStrategy
import com.gitlab.plugin.util.gitlabStatusRefresh
import com.intellij.openapi.project.Project

class ProjectContextService(val project: Project) {

  private val duoContext: DuoContextService by lazy { DuoContextService.instance }

  val duoApi by lazy {
    DuoApi(
      client = duoContext.duoHttpClient,
      telemetry = telemetry,
      duoSettings = duoContext.duoSettings,
      isLicensed = { codeSuggestionsLicense.isLicensed }
    )
  }

  private val serverService by lazy { GitLabServerService(duoApi) }

  // We don't want notifications or icon modifications when we check the version during project startup
  // These warnings are confusing to new users who have not set up their host/token yet
  val serverServiceWithoutCallbacks by lazy {
    GitLabServerService(
      DuoApi(
        duoContext.duoHttpClient.copy(
          exceptionHandler = {},
          onStatusChanged = object : DuoClient.DuoClientRequestListener {}
        ),
        duoSettings = duoContext.duoSettings
      )
    )
  }

  val completionStrategy by lazy { CompletionStrategy(duoApi, serverService) }

  val telemetry by lazy {
    val snowplowTracker = GitLabApplicationService.getInstance().snowplowTracker
    Telemetry(
      listOf(LogDestination(), SnowplowDestination(snowplowTracker)),
      isEnabled = { duoContext.duoSettings.telemetryEnabled }
    )
  }

  val graphQLApi by lazy {
    GraphQLApi(
      ApolloClientFactory(
        tokenProvider = duoContext.patProvider,
      )
    ).also { GraphQLApiDidChangeConfigurationListener(it) }
  }

  val codeSuggestionsLicense by lazy {
    CodeSuggestionsLicenseStatus(
      graphQLApi,
      onLicenseChanged = ::gitlabStatusRefresh
    )
  }

  companion object {
    lateinit var instance: ProjectContextService

    fun isInitialized() = this::instance.isInitialized

    fun init(project: Project): ProjectContextService {
      instance = ProjectContextService(project)
      return instance
    }
  }
}
