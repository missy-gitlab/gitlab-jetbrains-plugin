package com.gitlab.plugin.services

import com.gitlab.plugin.chat.exceptions.GitLabGraphQLResponseException
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import java.util.concurrent.ConcurrentHashMap

@Service(Service.Level.PROJECT)
class DuoEnabledProjectService(private val project: Project) {
  private val logger = logger<DuoEnabledProjectService>()
  private val gitLabProjectService = project.service<GitLabProjectService>()

  suspend fun isDuoEnabledForProject(): Boolean {
    val cacheEntry = duoEnabledProjectCache[project.name]
    if (cacheEntry != null) {
      return cacheEntry.second
    }
    var isDuoEnabled = true
    val projectPath = gitLabProjectService.getCurrentProjectPath()

    try {
      if (projectPath != null) {
        val graphQLProject = ProjectContextService.instance.graphQLApi.getProject(projectPath)
        if (graphQLProject?.duoFeaturesEnabled != null) {
          isDuoEnabled = graphQLProject.duoFeaturesEnabled
          duoEnabledProjectCache[project.name] = Pair(projectPath, isDuoEnabled)
        } else {
          logger.info("The project retrieved from GraphQL is null.")
        }
      }
    } catch (exception: GitLabGraphQLResponseException) {
      logger.info("GraphQL API call returned an error: ${exception.message}")
    }
    return isDuoEnabled
  }

  companion object {
    var duoEnabledProjectCache = ConcurrentHashMap<String, Pair<String, Boolean>>()
  }
}
