package com.gitlab.plugin.services.chat

import com.gitlab.plugin.graphql.ChatSubscription

object ChatChunkReconstructer {
  fun reconstructContent(chunks: Map<Int, ChatSubscription.AiCompletionResponse>): Pair<String, String> {
    var content = ""
    var contentHtml = ""
    for (i in 1..chunks.size) {
      val chunk = chunks.get(i)

      if (chunk != null) {
        content += chunk.content.orEmpty()
        contentHtml += chunk.contentHtml.orEmpty()
      } else {
        // Missing a chunk so there's no point in continuing
        break
      }
    }

    return Pair(content, contentHtml)
  }
}
