package com.gitlab.plugin.services.chat

import com.gitlab.plugin.chat.ChatController
import com.gitlab.plugin.chat.api.abstraction.ChatApiClient
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.chat.view.CefChatBrowser
import com.gitlab.plugin.chat.view.WebViewChatView
import com.intellij.openapi.Disposable
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindowManager
import javax.swing.JComponent

@Service(Service.Level.PROJECT)
internal class ChatService(project: Project) : Disposable {
  private val browser = CefChatBrowser()
  private val chatApiClient: ChatApiClient = project.service()
  private val controller = ChatController(
    chatApiClient = chatApiClient,
    chatView = WebViewChatView(
      chatBrowser = browser,
      toolWindowManager = ToolWindowManager.getInstance(project)
    ),
    project = project
  )

  val browserComponent: JComponent
    get() = browser.browserComponent

  suspend fun processNewUserPrompt(prompt: NewUserPromptRequest) = controller.processNewUserPrompt(prompt)

  override fun dispose() {
    browser.dispose()
  }
}
