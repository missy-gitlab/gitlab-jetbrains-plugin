package com.gitlab.plugin.util

fun removePrefix(str: String?, prefix: String?): String? = str?.removePrefix(prefix ?: "" as CharSequence)
