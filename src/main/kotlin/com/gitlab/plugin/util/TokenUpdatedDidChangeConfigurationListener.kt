package com.gitlab.plugin.util

import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.workspace.DidChangeConfigurationListener
import com.gitlab.plugin.workspace.DidChangeConfigurationParams
import com.gitlab.plugin.workspace.TokenSettings
import com.intellij.openapi.components.service
import com.intellij.util.application
import kotlinx.coroutines.runBlocking

class TokenUpdatedDidChangeConfigurationListener(
  private val userService: GitLabUserService = application.service<GitLabUserService>()
) : DidChangeConfigurationListener {
  override fun onConfigurationChange(params: DidChangeConfigurationParams) {
    val settings = params.settings.token
    if (settings is TokenSettings.Keychain) {
      TokenUtil.setToken(settings.value)
      runBlocking { userService.invalidateAndFetchCurrentUser() }

      if (ProjectContextService.isInitialized()) {
        gitlabStatusRefresh()
      }
    }
  }
}
