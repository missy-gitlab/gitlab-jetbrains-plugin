package com.gitlab.plugin.authentication

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.actions.chat.DUO_CHAT_TOOL_WINDOW_ID
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.components.service
import com.intellij.openapi.wm.ToolWindowManager
import com.intellij.util.application

@State(name = "GitLabAccount", storages = [Storage(value = "gitlab.xml")], reportStatistic = false)
class DuoPersistentSettings : PersistentStateComponent<DuoPersistentSettings.State> {
  data class State(
    var url: String = GitLabUtil.GITLAB_DEFAULT_URL,
    var enabled: Boolean = true,
    var telemetryEnabled: Boolean = true,
    var ignoreCertificateErrors: Boolean = false,
    var duoChatEnabled: Boolean = true,
    var suggestionFormattingEnabled: Boolean = BuildConfig.SUGGESTION_FORMATTING_SETTING_VISIBLE
  )

  private var state: State = State()

  override fun getState(): State = state

  override fun loadState(state: State) {
    this.state = state
  }

  fun toggleEnabled(): Boolean {
    enabled = !enabled

    return enabled
  }

  var url
    get() = state.url
    set(value) {
      state.url = value
    }

  /**
   * This property name is no longer meaningful now that we provide features other than code suggestions in the plugin.
   *
   * See this [tech debt issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/344).
   */
  @Deprecated("Use `codeSuggestionsEnabled` instead", ReplaceWith("codeSuggestionsEnabled"))
  var enabled
    get() = state.enabled
    set(value) {
      state.enabled = value
    }

  var codeSuggestionsEnabled by this::enabled

  var telemetryEnabled
    get() = state.telemetryEnabled
    set(value) {
      state.telemetryEnabled = value
    }

  var ignoreCertificateErrors
    get() = state.ignoreCertificateErrors
    set(value) {
      state.ignoreCertificateErrors = value
    }

  var duoChatEnabled
    get() = state.duoChatEnabled
    set(value) {
      state.duoChatEnabled = value
      if (ProjectContextService.isInitialized()) {
        val toolWindowManager = ToolWindowManager.getInstance(ProjectContextService.instance.project)
        val toolWindow = toolWindowManager.getToolWindow(DUO_CHAT_TOOL_WINDOW_ID)
        if (toolWindow?.isDisposed == false) {
          toolWindow.isAvailable = value
        }
      }
    }

  var suggestionFormattingEnabled
    get() = state.suggestionFormattingEnabled
    set(value) {
      state.suggestionFormattingEnabled = value
    }

  fun gitlabRealm() = if (url.matches(GITLAB_COM_REGEX)) GITLAB_REALM_SAAS else GITLAB_REALM_SELF_MANAGED

  companion object {
    const val GITLAB_REALM_SAAS = "saas"
    const val GITLAB_REALM_SELF_MANAGED = "self-managed"
    private val GITLAB_COM_REGEX = Regex("https?://gitlab\\.com")

    fun getInstance(): DuoPersistentSettings = application.service()
  }
}
