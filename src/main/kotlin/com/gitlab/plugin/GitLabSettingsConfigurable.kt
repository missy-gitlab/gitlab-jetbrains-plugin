package com.gitlab.plugin

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.ui.verifyServerConfiguration
import com.gitlab.plugin.util.LINKS
import com.gitlab.plugin.util.TokenUtil
import com.gitlab.plugin.workspace.DidChangeConfigurationListener
import com.gitlab.plugin.workspace.DidChangeConfigurationParams
import com.gitlab.plugin.workspace.TokenSettings
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.intellij.ide.BrowserUtil
import com.intellij.openapi.application.invokeLater
import com.intellij.openapi.options.BoundConfigurable
import com.intellij.openapi.options.Configurable
import com.intellij.openapi.options.ConfigurationException
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.components.BrowserLink
import com.intellij.ui.dsl.builder.Cell
import com.intellij.ui.dsl.builder.bindSelected
import com.intellij.ui.dsl.builder.bindText
import com.intellij.ui.dsl.builder.panel
import com.intellij.util.application
import java.util.*
import javax.swing.JLabel

class GitLabSettingsConfigurable : BoundConfigurable(GitLabBundle.message("settings.ui.group.name")) {

  private val settings = DuoPersistentSettings.getInstance()

  private var tokenText = TokenUtil.getToken() ?: ""
  private var urlText = settings.url

  private val bundle: ResourceBundle = ResourceBundle.getBundle("messages.GitLabBundle")
  private val urlErrorMessage = bundle.getString("settings.ui.error.url.invalid")
  private val tokenLabel = bundle.getString("settings.ui.gitlab.token")
  private val createTokenLabel = bundle.getString("settings.ui.gitlab.create-token")
  private val hostLabel = bundle.getString("settings.ui.gitlab.url")
  private val docsLabel = bundle.getString("settings.ui.gitlab.docs-link-label")
  private val telemetryLabel = bundle.getString("settings.ui.gitlab.enable-telemetry")
  private val verifySetupLabel = bundle.getString("settings.ui.gitlab.verify-setup")
  private val suggestionFormattingLabel = bundle.getString("settings.ui.gitlab.enable-suggestion-formatting")
  private val ignoreCertificateErrorsLabel = bundle.getString("settings.ui.gitlab.ignore-certificate-errors-label")
  private val ignoreCertificateErrorsDescription = bundle.getString(
    "settings.ui.gitlab.ignore-certificate-errors-description"
  )
  private val codeSuggestionsEnabledLabel = bundle.getString("settings.ui.gitlab.enable-duo-code-suggestions")
  private val duoChatEnabledLabel = bundle.getString("settings.ui.gitlab.enable-duo-chat")

  private var settingsPanel: DialogPanel? = null
  private var verifyLabel: Cell<JLabel>? = null
  private lateinit var errorReportingInstructions: Cell<BrowserLink>

  private val didConfigurationChangeListener = application
    .messageBus
    .syncPublisher(DidChangeConfigurationListener.DID_CHANGE_CONFIGURATION_TOPIC)

  @Suppress("LongMethod")
  override fun createPanel(): DialogPanel {
    var tokenValue: String = tokenText

    settingsPanel = panel {
      row {
        browserLink(docsLabel, LINKS.CODE_SUGGESTIONS_SETUP_DOCS_URL)
      }

      groupRowsRange("Connection") {
        row(hostLabel) {
          textField().bindText(settings::url)
            .addValidationRule(urlErrorMessage) { it.text.isBlank() }
            .onChanged { urlText = it.text }
        }
        row {
          checkBox(ignoreCertificateErrorsLabel).bindSelected(settings::ignoreCertificateErrors).also { checkbox ->
            comment(ignoreCertificateErrorsDescription).also { warning ->
              warning.visible(settings.ignoreCertificateErrors)
              checkbox.onChanged {
                warning.visible(it.isSelected)
                settings.ignoreCertificateErrors = it.isSelected
              }
            }
          }
        }

        row(tokenLabel) {
          passwordField()
            .bindText(::tokenText)
            .focused()
            .onChanged { component -> tokenValue = String(component.password) }
          button(createTokenLabel) { BrowserUtil.open("${settings.url}${LINKS.CREATE_TOKEN_PATH}") }
        }

        twoColumnsRow({
          button(verifySetupLabel) {
            verifyLabel!!.component.text = "Verifying..."

            invokeLater {
              verifyServerConfiguration(urlText, tokenValue, verifyLabel!!.component, errorReportingInstructions)
            }
          }
        }) {
          verifyLabel = label("")
        }

        twoColumnsRow({}, {
          errorReportingInstructions = browserLink(
            "How to report errors",
            "https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin#reporting-issues-or-providing-feedback"
          ).visible(false)
        })
      }

      groupRowsRange("Features") {
        row {
          checkBox(codeSuggestionsEnabledLabel).bindSelected(settings::codeSuggestionsEnabled)
        }

        row {
          checkBox(duoChatEnabledLabel).bindSelected(settings::duoChatEnabled)
        }
      }

      groupRowsRange("Advanced") {
        row {
          checkBox(telemetryLabel).bindSelected(settings::telemetryEnabled)
        }

        row {
          checkBox(suggestionFormattingLabel)
            .visible(BuildConfig.SUGGESTION_FORMATTING_SETTING_VISIBLE)
            .bindSelected(settings::suggestionFormattingEnabled)
        }
      }
    }

    return settingsPanel as DialogPanel
  }

  override fun apply() {
    val panel = settingsPanel ?: return
    val validationMessages = panel.validateAll()

    if (validationMessages.isEmpty()) {
      super.apply()

      didConfigurationChangeListener.onConfigurationChange(
        params = DidChangeConfigurationParams(
          settings = WorkspaceSettings(url = urlText, token = TokenSettings.Keychain(tokenText))
        )
      )
    } else {
      throw ConfigurationException(validationMessages.joinToString(separator = "\n") { it.message })
    }
  }
}

fun GitLabSettingsConfigurable.asBeta(): Configurable =
  object : Configurable by this, Configurable.Beta {}
