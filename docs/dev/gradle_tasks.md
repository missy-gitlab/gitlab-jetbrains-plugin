# Gradle tasks used in this project

| Name                    | Description                                                                                                                                                                              |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `test`                  | Run unit tests. For information on running end-to-end tests, see [end-to-end testing docs](end_to_end_testing.md).                                                                       |
| `detektMain detektTest` | Run the detekt linter for the `main` and `test` source sets using the `detekt.yml` configuration. Use the `--auto-correct` option to allow rules to autocorrect code if they support it. |
| `assemble`              | Build the plugin into a `zip` file that can be installed into the IDE. The file is written to `build/distributons/`.                                                                     |
| `runIde`                | Build the plugin and open a separate IDE to run it. Useful for debugging the plugin with breakpoints.                                                                                    |
| `patchChangelog`        | Update the `Unreleased` section of `Changelog.md` to the version (`plugin.version`) specified in `gradle.properties`.                                                                    |
| `npmRunBuild`           | Depends on `npmInstall`. Run `npm run build` in the `webview` directory.                                                                                                                 |
| `copyWebviewAssets`     | Depends on `npmRunBuild`. Copy the contents of `webview/dist` to `src/main/resources/webview`.                                                                                           |
