# GitLab Duo JetBrains plugin end-to-end tests

Utilises [intellij-ui-test-robot](https://github.com/JetBrains/intellij-ui-test-robot) to test the GitLab Duo JetBrains plugin using a real
instance of IntelliJ.

To launch the IDE for testing, as it is blocking it must be run in a separate process to the tests:

```shell
./gradlew runIdeForUiTests
```

To run the end-to-end tests set the project property `e2eTests`. By doing this, we execute only the e2e tests and exclude all other tests.
The E2E tests utilise a [test suite file](../../src/test/kotlin/com/gitlab/plugin/e2eTest/suites/E2ETestSuite.kt), to ensure unauthenticated tests
are performed first, followed by authenticated tests.

The E2E tests require the following environment variables to be set: `TEST_GITLAB_HOST` and `TEST_ACCESS_TOKEN`. Please ensure
the token's user has code suggestions enabled.

```shell
TEST_GITLAB_HOST="https://staging.gitlab.com" TEST_ACCESS_TOKEN="<insert_token>" ./gradlew -Pe2eTests test --tests E2ETestSuite
```

Using `cleanTest` will ensure the tests are run even if they are up-to-date:

```shell
TEST_GITLAB_HOST="https://staging.gitlab.com" TEST_ACCESS_TOKEN="<insert_token>" ./gradlew -Pe2eTests cleanTest test --tests E2ETestSuite
```

To run the end-to-end tests in one command:

```shell
TEST_GITLAB_HOST="https://staging.gitlab.com" TEST_ACCESS_TOKEN="<insert_token>" ./gradlew clean :runIdeForUiTests & ./gradlew -Pe2eTests cleanTest test --tests E2ETestSuite
```

Video recordings are saved in the `video` directory for failed tests.

## FAQs

### Why on my Mac is the UI click not working?

On Mac computers, you will need to allow the IDE to control it:
```Security & Privacy``` -> ```Accessibility``` -> ```Allow the apps below to control your computer``` and select the
IDE you are working with.
